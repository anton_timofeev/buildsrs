package com.srs.controller;

import com.srs.app.AppContext;
import com.srs.config.SRSConfiguration;
import com.srs.service.SRSValidationService;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.List;
import java.util.Set;

public class CheckSRSController {
    private Stage checkSRSStage;

    private ListView<String> checkSRSResults = new ListView<>();

    private SRSValidationService validationService = SRSValidationService.getInstance();

    @FXML
    public void okClicked(Event event) {
        if (checkSRSStage != null) {
            checkSRSStage.close();
        }
    }

    public Stage getCheckSRSStage() {
        return checkSRSStage;
    }

    public Boolean check() {
        return check(false);
    }

    public Boolean check(boolean isBuild) {
        AppContext context = AppContext.getInstance();
        Boolean errorsDetected = false;
        checkSRSResults.getItems().removeIf(o -> o != null);
        //Checks Responses
        //checkSRSResults.getItems().add("Checking Responses...");
        //Indicate if table is empty
        long srsQuestionsAmount = SRSConfiguration.getInstance().getQuestions().size();
        if (srsQuestionsAmount == 0) {
            errorsDetected = true;
            checkSRSResults.getItems().add("Empty table Requirements.");
        }
        long srsResponsesAmount = validationService.getSRSResponsesAmount();
        if (srsResponsesAmount == 0) {
            errorsDetected = true;
            checkSRSResults.getItems().add("Empty table Responses.");
        }
        List<String> reqNotFilled = validationService.getRequiredNotPopulated();
        if (!reqNotFilled.isEmpty()) {
            errorsDetected = true;
            reqNotFilled.forEach(each -> checkSRSResults.getItems().add("Missing entry for " + each));
        }
        List<String> notAllowedTbd = validationService.getTBDNotAllowedButTBD();
        if (!notAllowedTbd.isEmpty()) {
            errorsDetected = true;
            notAllowedTbd.forEach(each -> checkSRSResults.getItems().add("Invalid response in  " + each));
        }
        //Check all text against Problem Words and report
        Set<String> problemWordsDetected = validationService.validateResponseText();
        if (!problemWordsDetected.isEmpty()) {
            errorsDetected = null;
            problemWordsDetected.forEach(each -> checkSRSResults.getItems().add("Problem words detected in " + each));
        }

        //Check that response Name is valid in all rows
        // checkSRSResults.getItems().add("");
        //Check that response is valid value in all rows
        //Checks Glossary
        //Indicate if table is empty
        if (context.getGlossaryMap().isEmpty()) {
            errorsDetected = true;
            checkSRSResults.getItems().add("Empty table Glossary.");
        }
        //Check that there are no duplicate entries
        // TODO: it is invalid case for the application.

        //Checks Features
        // checkSRSResults.getItems().add("Checking Features...");
        //Indicate if table is empty
        if (context.getFeatures().isEmpty()) {
            errorsDetected = true;
            checkSRSResults.getItems().add("Empty table Features.");
        }
        //Check that there are no duplicate entries
        List<String> invalidFeatures = validationService.validateFeatures();
        if (!invalidFeatures.isEmpty()) {
            errorsDetected = true;
            invalidFeatures.forEach(each -> checkSRSResults.getItems().add("Feature missing priority or description: " + each));
        }
        //Check that there are no duplicate entries
        Set<String> duplicates = validationService.validateFeaturesDuplications();
        if (!duplicates.isEmpty()) {
            errorsDetected = true;
            checkSRSResults.getItems().add("Duplicate features detected: " + String.join(", ", duplicates));
        }
        //Check all text against Problem Words and report
        problemWordsDetected = validationService.validateFeaturesText();
        if (!problemWordsDetected.isEmpty()) {
            errorsDetected = true;
            checkSRSResults.getItems().add("Problem words are detected in features: " + String.join(", ", problemWordsDetected));
        }

        if (errorsDetected == null && isBuild) {
            return null;
        } else if (errorsDetected != null) {
            checkSRSResults.getItems().add("Fix errors listed above and proceed.");
            return false;
        }
        //At end;
        // checkSRSResults.getItems().add("");
        checkSRSResults.getItems().add("Build SRS statistics:");
        //BuildSRS shall display statistics:
        //Number of Glossary entries
        checkSRSResults.getItems().add("Number of Glossary entries: " + context.getGlossaryMap().size());
        //Number of Feature entries
        checkSRSResults.getItems().add("Number of Feature entries: " + context.getFeatures().size());
        //Number of Response entries
        checkSRSResults.getItems().add("Number of Response entries: " + context.getResponseList().size());
        //Number of TBD
        checkSRSResults.getItems().add("Number of TBD: " + context.getResponseList().values().stream().filter(each ->
                "TBD".equalsIgnoreCase(each.getResponse())).count());
        return true;
    }

    public CheckSRSController init(Stage checkSRSStage, boolean runCheck) {
        this.checkSRSStage = checkSRSStage;

        GridPane pane = (GridPane) this.checkSRSStage.getScene().lookup("#checkSRSForm");
        pane.getChildren().removeIf(each -> each != null);

        checkSRSResults.setEditable(false);
        pane.add(checkSRSResults, 0, 0, 2, 1);
        Button okButton = new Button("OK");
        okButton.setPrefWidth(120);
        okButton.setOnAction(event -> {
            checkSRSStage.close();
        });
        pane.add(okButton, 0, 1, 2, 1);
        if (runCheck) {
            check();
        }

        return this;
    }
}
