package com.srs.controller;

import com.srs.model.SRSQuestion;
import com.srs.views.DeletableCustomCell;
import com.sun.javafx.collections.ObservableListWrapper;
import com.sun.javafx.collections.ObservableSequentialListWrapper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class AddEditQuestionController implements Initializable {
    private static final List<String> YES_NO_VALUES_LIST = Arrays.asList("Yes", "No");
    // private static final List<String> QUESTION_TYPES_LIST = Arrays.asList("Text", "Select Single", "Select Multiple");
    private static final List<String> INSERT_TYPE_LIST = Arrays.asList("I", "P", "B");
    private static final List<String> QUESTION_TYPE_ABBREVIATION_LIST = Arrays.asList("T", "1", "M");
    private Stage currentStage;
    private Consumer<SRSQuestion> onClose;

    @FXML
    private ComboBox<String> fxTBDAllowed;
    @FXML
    private ComboBox<String> fxNotApplicableAllowed;
    @FXML
    private ComboBox<String> fxType;
    @FXML
    private ComboBox<String> fxEditQuestInsertType;
    @FXML
    private TextField fxEditQuestionSection;
    @FXML
    private TextField fxEditQuestionName;
    @FXML
    private TextField fxEditQuestionValues;
    @FXML
    private TextField fxEditQuestionDefault;
    @FXML
    private TextField fxEditQuestText;
    @FXML
    private TreeView fxEditQuestionValuesList;
    @FXML
    private TextArea fxEditQuestDesc;
    @FXML
    private TextArea fxQuestRequirementTemplate;

    private boolean isEdit;

    public boolean isEdit() {
        return isEdit;
    }

    public AddEditQuestionController setEdit(boolean edit) {
        isEdit = edit;
        return this;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fxTBDAllowed.setItems(new ObservableListWrapper<>(YES_NO_VALUES_LIST));
        fxNotApplicableAllowed.setItems(new ObservableListWrapper<>(YES_NO_VALUES_LIST));
        fxType.setItems(new ObservableListWrapper<>(QUESTION_TYPE_ABBREVIATION_LIST));
        fxEditQuestInsertType.setItems(new ObservableListWrapper<>(INSERT_TYPE_LIST));
        fxEditQuestionValuesList.setCellFactory(param -> new DeletableCustomCell());
        fxEditQuestionValuesList.setRoot(new TreeItem());
        fxEditQuestionValuesList.setShowRoot(false);
    }

    public void refresh() {
        fxEditQuestText.clear();
        fxEditQuestDesc.setText("");
        fxEditQuestionSection.clear();
        fxEditQuestionName.clear();
        fxEditQuestionValuesList.getRoot().getChildren().removeIf(p -> true);
        fxEditQuestionDefault.clear();
        fxType.setItems(new ObservableSequentialListWrapper<>(QUESTION_TYPE_ABBREVIATION_LIST));
        fxType.getSelectionModel().select(0);
        fxEditQuestInsertType.setItems(new ObservableListWrapper<>(INSERT_TYPE_LIST));
        fxEditQuestInsertType.getSelectionModel().select(0);
        fxNotApplicableAllowed.setItems(new ObservableSequentialListWrapper<>(YES_NO_VALUES_LIST));
        fxNotApplicableAllowed.getSelectionModel().select(0);
        fxTBDAllowed.setItems(new ObservableSequentialListWrapper<>(YES_NO_VALUES_LIST));
        fxTBDAllowed.getSelectionModel().select(0);
        fxQuestRequirementTemplate.clear();
    }

    public void setQuestion(SRSQuestion question) {
        fxEditQuestionSection.setText(question.getSection());
        fxEditQuestionName.setText(question.getName());
        fxEditQuestionValuesList.getRoot().getChildren().removeIf(p -> true);
        if (!question.getValues().isEmpty()) {
            fxEditQuestionValuesList.getRoot().getChildren().addAll(question.getValues().stream().map(TreeItem::new)
                    .collect(Collectors.toList()));
        }
        fxEditQuestionDefault.setText(question.getDefaultValue());
        fxType.setItems(new ObservableSequentialListWrapper<>(QUESTION_TYPE_ABBREVIATION_LIST));
        fxNotApplicableAllowed.setItems(new ObservableSequentialListWrapper<>(YES_NO_VALUES_LIST));
        fxTBDAllowed.setItems(new ObservableSequentialListWrapper<>(YES_NO_VALUES_LIST));

        fxType.getSelectionModel().select(question.getResponseType());
        fxNotApplicableAllowed.getSelectionModel().select(question.isNotApplicableAllowed() ? "Yes" : "No");
        fxTBDAllowed.getSelectionModel().select(question.isTbdAllowed() ? "Yes" : "No");
        fxEditQuestInsertType.getSelectionModel().select(question.getInsertIntoSRSType());

        fxEditQuestText.setText(question.getText());
        fxEditQuestDesc.setText(question.getDescription());
        fxQuestRequirementTemplate.setText(question.getRequirementTemplate());
    }

    public void addValue() {
        String value = fxEditQuestionValues.getText();
        if (value.length() == 0) {
            return;
        }
        fxEditQuestionValuesList.getRoot().getChildren().add(new TreeItem<>(value));
        fxEditQuestionValues.clear();
    }

    public void save() {
        SRSQuestion val = new SRSQuestion();
        val.setText(fxEditQuestText.getText());
        val.setResponseType(fxType.getSelectionModel().getSelectedItem());
        val.setSection(fxEditQuestionSection.getText());
        List<String> values = new ArrayList<>();
        fxEditQuestionValuesList
                .getRoot().getChildren().stream()
                .forEach(each -> values.add(((TreeItem) each).getValue().toString()));
        val.setValues(values);
        val.setDefaultValue(fxEditQuestionDefault.getText());
        val.setDescription(fxEditQuestDesc.getText());
        val.setNotApplicableAllowed("Yes".equalsIgnoreCase(fxNotApplicableAllowed.getSelectionModel().getSelectedItem()));
        val.setTbdAllowed("Yes".equalsIgnoreCase(fxTBDAllowed.getSelectionModel().getSelectedItem()));
        val.setName(fxEditQuestionName.getText());
        val.setRequirementTemplate(fxQuestRequirementTemplate.getText());
        val.setInsertIntoSRSType(fxEditQuestInsertType.getSelectionModel().getSelectedItem());
        if (!val.validateState()) {
            INVALID_QUESTION_ALERT.showAndWait();
            return;
        }

        onClose.accept(val);
        currentStage.close();
    }

    private static final Alert INVALID_QUESTION_ALERT = new Alert(Alert.AlertType.WARNING,
            "Invalid requirement specification. Please fix problems to proceed", ButtonType.OK);

    public void close() {
        currentStage.close();
    }

    public Stage getCurrentStage() {
        return currentStage;
    }

    public AddEditQuestionController setCurrentStage(Stage currentStage) {
        this.currentStage = currentStage;
        return this;
    }

    public Consumer<SRSQuestion> getOnClose() {
        return onClose;
    }

    public AddEditQuestionController setOnClose(Consumer<SRSQuestion> onClose) {
        this.onClose = onClose;
        return this;
    }
}
