package com.srs.controller;

import com.srs.config.SRSConfiguration;
import com.srs.model.SRSQuestion;
import com.srs.views.CustomEvent;
import com.srs.views.DeletableCustomCell;
import com.srs.views.DeletableCustomTableCell;
import com.srs.views.PreferenceUpdateEvent;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeSortMode;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class PreferenceController implements Initializable {
    private Stage preferencesStage;
    private Stage addNewQuestionStage;
    private Stage editQuestionStage;
    @FXML
    private TreeTableView<SRSQuestion> fxQuestions;
    @FXML
    private TreeView<String> fxIgnoreWords;
    @FXML
    private TreeView<String> fxProblemWords;

    @FXML
    private TreeTableColumn<SRSQuestion, Button> fxQuestionsDelete;
    @FXML
    private TreeTableColumn<SRSQuestion, String> fxQuestionsSection;
    @FXML
    private TreeTableColumn<SRSQuestion, String> fxQuestionsName;
    @FXML
    private TreeTableColumn<SRSQuestion, String> fxQuestionsResponseType;
    @FXML
    private TreeTableColumn<SRSQuestion, String> fxQuestionsValues;
    @FXML
    private TreeTableColumn<SRSQuestion, String> fxQuestionsDefault;
    @FXML
    private TreeTableColumn<SRSQuestion, Boolean> fxQuestionsTBD;
    @FXML
    private TreeTableColumn<SRSQuestion, Boolean> fxQuestionsNotAppl;
    @FXML
    private TreeTableColumn<SRSQuestion, String> fxQuestionsReqTemplate;
    @FXML
    private TreeTableColumn<SRSQuestion, String> fxQuestionsText;
    @FXML
    private TreeTableColumn<SRSQuestion, String> fxQuestionsDesc;
    @FXML
    private TreeTableColumn<SRSQuestion, String> fxQuestionsInsertType;

    @FXML
    private TextField newIgnoreWord;
    @FXML
    private TextField newProblemWord;

    private AddEditQuestionController editQuestionController;
    private AddEditQuestionController addQuestionController;

    private List<SRSQuestion> questionsList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fxQuestionsDelete.setCellFactory(param -> new DeletableCustomTableCell());
        fxQuestionsDelete.setSortable(false);
        fxQuestionsSection.setCellValueFactory(new TreeItemPropertyValueFactory("section"));
        fxQuestionsSection.setSortable(false);
        fxQuestionsDefault.setCellValueFactory(new TreeItemPropertyValueFactory("defaultValue"));
        fxQuestionsDefault.setSortable(false);
        fxQuestionsName.setCellValueFactory(new TreeItemPropertyValueFactory("name"));
        fxQuestionsName.setSortable(false);

        //fxQuestionsNotAppl.setCellFactory(booleanCellFactory);
        fxQuestionsNotAppl.setCellValueFactory(new TreeItemPropertyValueFactory("notApplicableAllowed"));
        fxQuestionsNotAppl.setSortable(false);
        //fxQuestionsTBD.setCellFactory(booleanCellFactory);
        fxQuestionsTBD.setCellValueFactory(new TreeItemPropertyValueFactory("tbdAllowed"));
        fxQuestionsTBD.setSortable(false);

        fxQuestionsReqTemplate.setCellValueFactory(new TreeItemPropertyValueFactory("requirementTemplate"));
        fxQuestionsReqTemplate.setSortable(false);
        fxQuestionsResponseType.setCellValueFactory(new TreeItemPropertyValueFactory("responseType"));
        fxQuestionsResponseType.setSortable(false);
        fxQuestionsText.setCellValueFactory(new TreeItemPropertyValueFactory("text"));
        fxQuestionsText.setSortable(false);
        fxQuestionsValues.setCellValueFactory(new TreeItemPropertyValueFactory("values"));
        fxQuestionsValues.setSortable(false);
        fxQuestionsDesc.setCellValueFactory(new TreeItemPropertyValueFactory("description"));
        fxQuestionsDesc.setSortable(false);
        fxQuestionsInsertType.setCellValueFactory(new TreeItemPropertyValueFactory("insertIntoSRSType"));
        fxQuestionsInsertType.setSortable(false);

        fxIgnoreWords.setCellFactory(param -> new DeletableCustomCell());
        fxProblemWords.setCellFactory(param -> new DeletableCustomCell());

        INFORMATION.setHeaderText("");
    }

    public PreferenceController setPreferencesStage(Stage preferencesStage) {
        this.preferencesStage = preferencesStage;
        this.preferencesStage.setOnCloseRequest(event -> {
            closePreferenceWindow(preferencesStage);
        });
        fxQuestions.addEventHandler(CustomEvent.TABLE_ROW_DELETED, event -> {
            questionsList.remove((event).getParameter());
        });
        return this;
    }

    private void closePreferenceWindow(Stage preferencesStage) {
        if (!SRSConfiguration.getInstance().isEverythingStored()) {
            Optional<ButtonType> res = INFORMATION.showAndWait();
            if (res.isPresent() && res.get() == ButtonType.YES) {
                try {
                    save();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                preferencesStage.hide();
            }
        } else {
            preferencesStage.hide();
        }
    }

    private Alert INFORMATION = new Alert(Alert.AlertType.INFORMATION,
            "Do you want to save changes?", ButtonType.YES, ButtonType.NO);

    public void close() {
        closePreferenceWindow(preferencesStage);
    }

    public void save() throws IOException {
        SRSConfiguration config = SRSConfiguration.getInstance();
        config.updateQuestions(fxQuestions.getRoot().getChildren().stream().map(TreeItem::getValue).collect(Collectors.toList()));
        config.updateIgnoreWords(fxIgnoreWords.getRoot().getChildren().stream().map(TreeItem::getValue).collect(Collectors.toList()));
        config.updateProblemWords(fxProblemWords.getRoot().getChildren().stream().map(TreeItem::getValue).collect(Collectors.toList()));

        preferencesStage.fireEvent(new PreferenceUpdateEvent());

        // INFORMATION.showAndWait();
        preferencesStage.hide();
    }

    public void refreshForm() {
        // clear questions table
        SRSConfiguration.getInstance().setEverythingStored(true);
        fxQuestions.refresh();
        fxQuestions.setShowRoot(false);
        fxQuestions.setRoot(new TreeItem<>(new SRSQuestion()));
        SRSConfiguration.getInstance().getQuestions().stream()
                .forEachOrdered(quest -> fxQuestions.getRoot().getChildren().add(new TreeItem<>(quest)));


        fxIgnoreWords.setRoot(new TreeItem<>(""));
        fxIgnoreWords.setShowRoot(false);
        SRSConfiguration.getInstance().getIgnoreWords().stream()
                .forEachOrdered(each -> fxIgnoreWords.getRoot().getChildren().add(new TreeItem<>(each)));

        fxProblemWords.setRoot(new TreeItem<>(""));
        fxProblemWords.setShowRoot(false);
        SRSConfiguration.getInstance().getProblemWords().stream()
                .forEachOrdered(each -> fxProblemWords.getRoot().getChildren().add(new TreeItem<>(each)));
        questionsList = fxQuestions.getRoot().getChildren().stream().map(TreeItem::getValue).collect(Collectors.toList());
    }

    public void addNewQuestion(Event event) throws IOException {
        if (addNewQuestionStage != null) {
            addQuestionController.refresh();
            addNewQuestionStage.show();
            return;
        }
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/srs/views/editknowledgebaseform.fxml"));
        Parent root = loader.load();
        addQuestionController = loader.getController();
        addQuestionController.setCurrentStage(dialog);
        addQuestionController.setOnClose(val -> {
            questionsList.add(val);
            fxQuestions.getRoot().getChildren().add(new TreeItem<>(val));
            SRSConfiguration.getInstance().setEverythingStored(false);
        });
        dialog.setTitle("Create new entry for Knowledge Base");
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setResizable(false);
        Scene scene = new Scene(root);
        dialog.setScene(scene);
        addQuestionController.refresh();
        dialog.show();

        // PreferenceController controller = loader.getController();
        // controller.setPreferencesStage(dialog);
        addNewQuestionStage = dialog;
    }

    public void editQuestion(Event event) throws IOException {

        TreeTableView.TreeTableViewSelectionModel<SRSQuestion> selectionModel = fxQuestions.getSelectionModel();
        if (selectionModel == null) {
            return;
        }
        TreeItem<SRSQuestion> selectedItem = selectionModel.getSelectedItem();
        if (selectedItem == null) {
            return;
        }
        SRSQuestion question = selectedItem.getValue();
        if (question == null) {
            return;
        }

        if (editQuestionStage != null) {
            editQuestionController.setQuestion(question);
            editQuestionStage.show();
            return;
        }


        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/srs/views/editknowledgebaseform.fxml"));
        Parent root = loader.load();
        editQuestionController = loader.getController();
        editQuestionController.setQuestion(question);
        editQuestionController.setCurrentStage(dialog);
        editQuestionController.setOnClose(val -> {
            int selectedIndex = fxQuestions.getSelectionModel().getSelectedIndex();
            questionsList.set(selectedIndex, val);
            fxQuestions.getSelectionModel().getTreeTableView().getRoot().getChildren().set(selectedIndex, new TreeItem<>(val));
            SRSConfiguration.getInstance().setEverythingStored(false);
        });
        dialog.setTitle("Edit Knowledge Base");
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setResizable(false);
        Scene scene = new Scene(root);
        dialog.setScene(scene);
        dialog.show();

        // PreferenceController controller = loader.getController();
        // controller.setPreferencesStage(dialog);
        editQuestionStage = dialog;
    }

    public void addIgnoreWord(Event event) {
        String newWord = newIgnoreWord.getText();
        if (newWord == null || newWord.length() == 0) {
            return;
        }
        fxIgnoreWords.getRoot().getChildren().add(new TreeItem<>(newWord));
        fxIgnoreWords.getRoot().getChildren().sort(Comparator.comparing(a -> a.getValue()));
        SRSConfiguration.getInstance().setEverythingStored(false);
        newIgnoreWord.clear();
    }

    public void addProblemWord(Event event) {
        String newWord = newProblemWord.getText();
        if (newWord == null || newWord.length() == 0) {
            return;
        }
        fxProblemWords.getRoot().getChildren().add(new TreeItem<>(newWord));
        fxProblemWords.getRoot().getChildren().sort(Comparator.comparing(a -> a.getValue()));
        SRSConfiguration.getInstance().setEverythingStored(false);
        newProblemWord.clear();
    }

    @FXML
    public void moveUp() {
        if (fxQuestions.getSelectionModel() == null) {
            return;
        }
        TreeItem<SRSQuestion> selectedItem = fxQuestions.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }
        SRSQuestion value = fxQuestions.getSelectionModel().getSelectedItem().getValue();
        int currentIndex = fxQuestions.getSelectionModel().getSelectedIndex();
        int prevIndex = currentIndex - 1;
        if (value != null && currentIndex > 0 && prevIndex >= 0) {
            changeQuestionsSelection(currentIndex, prevIndex);
        }
    }

    @FXML
    public void moveDown() {
        if (fxQuestions.getSelectionModel() == null) {
            return;
        }
        TreeItem<SRSQuestion> selectedItem = fxQuestions.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }
        SRSQuestion value = fxQuestions.getSelectionModel().getSelectedItem().getValue();
        int currentIndex = fxQuestions.getSelectionModel().getSelectedIndex();
        int nextIndex = currentIndex + 1;
        if (value != null && currentIndex >= 0 && questionsList.size() > nextIndex) {
            changeQuestionsSelection(currentIndex, nextIndex);
        }
    }

    private void changeQuestionsSelection(int currentIndex, int prevIndex) {
        SRSQuestion selected = questionsList.get(currentIndex);
        SRSQuestion prev = questionsList.get(prevIndex);
        questionsList.set(prevIndex, selected);
        questionsList.set(currentIndex, prev);

        fxQuestions.getRoot().getChildren().set(prevIndex, new TreeItem<>(selected));
        fxQuestions.getRoot().getChildren().set(currentIndex, new TreeItem<>(prev));
        fxQuestions.getSelectionModel().select(prevIndex);
        SRSConfiguration.getInstance().setEverythingStored(false);
    }

    /**
     * A custom cell that shows a checkbox, label and button in the
     * TreeCell.
     */

    Callback<TreeTableColumn<SRSQuestion, Boolean>, TreeTableCell<SRSQuestion, Boolean>> booleanCellFactory =
            new Callback<TreeTableColumn<SRSQuestion, Boolean>, TreeTableCell<SRSQuestion, Boolean>>() {
                @Override
                public TreeTableCell<SRSQuestion, Boolean> call(TreeTableColumn<SRSQuestion, Boolean> p) {
                    BooleanCell booleanCell = new BooleanCell();

                    return booleanCell;
                }
            };

    class BooleanCell extends TreeTableCell<SRSQuestion, Boolean> {
        private CheckBox checkBox;

        public BooleanCell() {
            checkBox = new CheckBox();
            checkBox.setDisable(true);
            checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (isEditing())
                        commitEdit(newValue == null ? false : newValue);
                }
            });
            this.setGraphic(checkBox);
            //this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            this.setEditable(false);
        }

        @Override
        public void startEdit() {
            super.startEdit();
            if (isEmpty()) {
                return;
            }
            checkBox.setDisable(false);
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            checkBox.setDisable(true);
        }

        public void commitEdit(Boolean value) {
            super.commitEdit(value);
            checkBox.setDisable(true);
        }

        @Override
        public void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);

            if (!isEmpty()) {
                checkBox.setSelected(item);
            } else {
                // checkBox.setVisible(false);
            }
        }
    }
}
