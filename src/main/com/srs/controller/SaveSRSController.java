package com.srs.controller;

import com.srs.app.AppContext;
import com.srs.config.SRSConfiguration;
import com.srs.model.SRSMeta;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class SaveSRSController {
    private Stage saveSRSStage;

    private static Alert FILE_EMPTY_WARNING = new Alert(Alert.AlertType.WARNING, "Please choose destination where file should be stored.", ButtonType.OK);
    private static Alert REQUIRED_FIELD_WARNING = new Alert(Alert.AlertType.WARNING, "All fields should be filled.", ButtonType.OK);
    // private static Alert FILE_EXIST_WARNING = new Alert(Alert.AlertType.WARNING, "File already exists. Do you want to overwrite it.", ButtonType.OK, ButtonType.CANCEL);

    @FXML
    private TextField fxSaveSRSFile;
    @FXML
    private CheckBox fxSaveSRSInclude;

    @FXML
    public void close(Event event) {
        saveSRSStage.close();
    }

    @FXML
    public void selectFile(Event event) {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Html", "*.html"));
        fc.setInitialDirectory(new File(SRSConfiguration.getInstance().getDefaultDirectory()));
        File file = fc.showSaveDialog(saveSRSStage);
        if (file != null) {
            fxSaveSRSFile.setText(file.getAbsolutePath());
        }
    }

    @FXML
    public void saveSRS(Event event) throws IOException {

        if (fxSaveSRSFile.getText().length() == 0) {
            FILE_EMPTY_WARNING.show();
        } else {
            save();
        }
    }


    private Stage buildSRSDialog = null;
    private BuildSRSController buildSRSController;

    private void save() throws IOException {
        if (buildSRSDialog != null) {
            buildSRSController.reset();
            buildSRSDialog.show();
            actualSave();
            return;
        }
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/srs/views/buildsrsform.fxml"));
        Parent root = loader.load();
        dialog.setResizable(false);
        dialog.setTitle("Build SRS.");
        dialog.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(root);
        dialog.setScene(scene);
        dialog.show();

        buildSRSController = loader.getController();
        buildSRSController.setBuildSRSStage(dialog);
        buildSRSDialog = dialog;

        actualSave();
    }

    private void actualSave() {
        saveSRSStage.hide();
        AppContext context = AppContext.getInstance();
        SRSConfiguration config = SRSConfiguration.getInstance();

        SRSMeta meta = new SRSMeta();
        meta.setDateCreated(new Date());
        meta.setFile(fxSaveSRSFile.getText());
        meta.setIncludeNegative(fxSaveSRSInclude.isSelected());
        meta.setProductName(context.getProductName());

        buildSRSController.startBuilding(meta,
                config.getQuestions(),
                context.getResponseList(),
                context.getGlossaryMap(),
                context.getFeatures());
    }

    public void reset() {
        fxSaveSRSFile.clear();
        fxSaveSRSInclude.setSelected(true);
    }

    public void setStage(Stage saveSRSStage) {
        this.saveSRSStage = saveSRSStage;
    }
}
