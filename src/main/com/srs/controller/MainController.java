package com.srs.controller;

import com.srs.app.AppContext;
import com.srs.app.properties.AppGlobalProperties;
import com.srs.config.SRSConfiguration;
import com.srs.config.SRSProperties;
import com.srs.model.SRSFeature;
import com.srs.model.SRSGlossary;
import com.srs.service.SRSBuildServiceHTMLImpl;
import com.srs.service.SRSGlossaryService;
import com.srs.service.SRSProductService;
import com.srs.views.DeletableCustomCell;
import com.srs.views.PreferenceUpdateEvent;
import com.srs.views.SelectProductForm;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    private static final ImageView REFRESH_IMAGE_VIEW =
            new ImageView(new Image(MainController.class.getClassLoader().getResourceAsStream(("img/refresh.png"))));

    private Stage mainWindow;
    private SelectProductForm productsWindow;
    @FXML
    private GridPane loadingPane;
    private final Label label = new Label("Please select item..");
    private HBox makeChoiceBox = new HBox(label);
    private final Label glossaryLabel = new Label("Please select item..");
    private HBox glossaryMakeChoiceBox = new HBox(glossaryLabel);
    @FXML
    private GridPane fxFeaturesDetails;
    @FXML
    private GridPane fxGlossaryDetails;
    @FXML
    private TreeView fxGlossaryList;
    @FXML
    private TextField fxGlossaryTerm;
    @FXML
    private TextArea fxDefinition;
    @FXML
    private Button fxRefresh;


    private SRSGlossaryService glossaryService = SRSGlossaryService.getInstance();

    @FXML
    public void onShown() {

        loadingPane.setVisible(false);
    }

    public MainController setProductsWindow(SelectProductForm productsWindow) {
        this.productsWindow = productsWindow;
        return this;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        label.setAlignment(Pos.CENTER);
        glossaryLabel.setAlignment(Pos.CENTER);
        makeChoiceBox.setBackground(new Background(new BackgroundFill(Paint.valueOf("grey"), CornerRadii.EMPTY, Insets.EMPTY)));
        makeChoiceBox.setAlignment(Pos.CENTER);
        fxFeaturesDetails.add(makeChoiceBox, 1, 1);
        glossaryMakeChoiceBox.setBackground(new Background(new BackgroundFill(Paint.valueOf("grey"), CornerRadii.EMPTY, Insets.EMPTY)));
        glossaryMakeChoiceBox.setAlignment(Pos.CENTER);
        fxGlossaryDetails.add(glossaryMakeChoiceBox, 1, 1);
    }

    public Stage getMainWindow() {
        return mainWindow;
    }

    public MainController setMainWindow(Stage mainWindow) {
        this.mainWindow = mainWindow;
        return this;
    }

    // TODO: refactor
    private Stage aboutDialog = null;
    private Stage licenseDialog = null;
    private Stage checkSRSDialog = null;
    private Stage buildSRSDialog = null;

    private Stage preferencesDialog = null;
    private PreferenceController preferencesController = null;
    private CheckSRSController checkSRSController;

    private SRSBuildServiceHTMLImpl service = SRSBuildServiceHTMLImpl.getInstance();
    private SRSProductService productService = SRSProductService.getInstance();

    public void init() {
        fxGlossaryList.setRoot(new TreeItem());
        fxGlossaryList.setShowRoot(false);

        fxFeatureTreeView.setRoot(new TreeItem());
        fxFeatureTreeView.setShowRoot(false);

        List<SRSFeature> features = AppContext.getInstance().getFeatures();
        fxFeatureDescription.textProperty().addListener((observable, oldValue, newValue) -> {
            String value = fxFeatureTreeView.getSelectionModel().getSelectedItem().getValue();
            if (value != null && value.length() > 0) {
                features.get(features.indexOf(new SRSFeature().setName(value))).setDescription(newValue);
                AppContext.getInstance().setEverythingStored(false);
            }
        });
        fxFeaturePriority.textProperty().addListener((observable, oldValue, newValue) -> {
            String value = fxFeatureTreeView.getSelectionModel().getSelectedItem().getValue();
            if (value != null && value.length() > 0) {
                features.get(features.indexOf(new SRSFeature().setName(value))).setPriority(newValue);
                AppContext.getInstance().setEverythingStored(false);
            }
        });
        fxFeatureResponse.textProperty().addListener((observable, oldValue, newValue) -> {
            String value = fxFeatureTreeView.getSelectionModel().getSelectedItem().getValue();
            if (value != null && value.length() > 0) {
                features.get(features.indexOf(new SRSFeature().setName(value))).setStimulusResponse(newValue);
                AppContext.getInstance().setEverythingStored(false);
            }
        });

        fxFeatureTreeView.setCellFactory(param -> {
            DeletableCustomCell cell = new DeletableCustomCell();
            cell.setOnMouseClicked(event -> {
                if (!cell.isEmpty()) {
                    makeChoiceBox.setVisible(false);
                    TreeItem<String> item = cell.getTreeItem();
                    int index = features.indexOf(new SRSFeature().setName(item.getValue()));
                    SRSFeature srsFeature;
                    if (index < 0) {
                        srsFeature = new SRSFeature().setName(item.getValue());
                        features.add(srsFeature);
                    } else {
                        srsFeature = features.get(index);
                    }

                    srsFeature.setName(item.getValue());
                    fxFeatureName.setText(item.getValue());
                    fxFeatureDescription.setText(srsFeature.getDescription() == null ? "" : srsFeature.getDescription());
                    fxFeaturePriority.setText(srsFeature.getPriority() == null ? "" : srsFeature.getPriority());
                    fxFeatureResponse.setText(srsFeature.getStimulusResponse() == null ? "" : srsFeature.getStimulusResponse());
                }
            });
            cell.addEventHandler(DeletableCustomCell.ITEM_DELETED, event -> {
                AppContext context = AppContext.getInstance();
                context.getFeatures().remove(new SRSFeature().setName(cell.getTreeItem().getValue()));
                AppContext.getInstance().setEverythingStored(false);
                fxFeatureTreeView.getSelectionModel().clearSelection();
                makeChoiceBox.setVisible(true);
            });
            return cell;
        });
        fxGlossaryList.setCellFactory(param -> {
            DeletableCustomCell cell = new DeletableCustomCell();
            cell.setOnMouseClicked(event -> {
                if (!cell.isEmpty()) {
                    glossaryMakeChoiceBox.setVisible(false);
                    AppContext context = AppContext.getInstance();
                    TreeItem<String> item = cell.getTreeItem();
                    SRSGlossary srsGlossary = context.getGlossaryMap().get(item.getValue());
                    fxGlossaryTerm.setText(item.getValue());
                    fxDefinition.setText(srsGlossary.getDefinition());
                }
            });
            cell.addEventHandler(DeletableCustomCell.ITEM_DELETED, event -> {
                AppContext context = AppContext.getInstance();
                context.getGlossaryMap().remove(cell.getTreeItem().getValue());
                AppContext.getInstance().setEverythingStored(false);
                fxGlossaryList.getSelectionModel().clearSelection();
                glossaryMakeChoiceBox.setVisible(true);
            });
            return cell;
        });
        featuresMap = AppContext.getInstance().getFeatures();

        REFRESH_IMAGE_VIEW.setFitHeight(20);
        REFRESH_IMAGE_VIEW.setFitWidth(20);
        fxRefresh.setGraphic(REFRESH_IMAGE_VIEW);

        initTooltips();
        makeChoiceBox.setVisible(true);
        glossaryMakeChoiceBox.setVisible(true);
    }

    private void initTooltips() {
        fxRefresh.setTooltip(new Tooltip("Refresh Glossary list"));


        fxGlossaryTerm.setTooltip(new Tooltip("Glossary entry. Can be edited"));
        fxDefinition.setTooltip(new Tooltip("Glossary Term description"));
    }

    @FXML
    public void saveDefinition(Event event) {
        AppContext context = AppContext.getInstance();
        String valueSelected = ((TreeItem) fxGlossaryList.getSelectionModel().getSelectedItem()).getValue().toString();
        if (valueSelected != null) {
            SRSGlossary srsGlossary = context.getGlossaryMap().get(valueSelected);
            if (!valueSelected.equalsIgnoreCase(fxGlossaryTerm.getText())) {
                srsGlossary.setTerm(fxGlossaryTerm.getText());
                context.getGlossaryMap().remove(valueSelected);
                context.getGlossaryMap().put(srsGlossary.getTerm(), srsGlossary);
                ((TreeItem) fxGlossaryList.getSelectionModel().getSelectedItem()).setValue(srsGlossary.getTerm());
            }
            srsGlossary.setDefinition(fxDefinition.getText());
            context.setEverythingStored(false);
        }
    }

    @FXML
    public void clearDefinition(Event event) {
        fxDefinition.clear();
    }

    @FXML
    public void aboutClick(Event event) throws IOException {
        if (aboutDialog != null) {
            aboutDialog.show();
            return;
        }
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/srs/views/aboutform.fxml"));
        Parent root = loader.load();
        //dialog.initOwner(stage);
        dialog.setResizable(false);
        dialog.setTitle("About");
        dialog.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(root);
        dialog.setScene(scene);

        AboutController controller = loader.getController();
        controller.setStage(dialog);

        ((Label) scene.lookup("#aboutLabel")).setText(SRSProperties.getAboutText());
        dialog.show();

        aboutDialog = dialog;
    }

    private Alert LIC_INFO = new Alert(Alert.AlertType.INFORMATION, "Your license has been already accepted.", ButtonType.OK);

    @FXML
    public void licenseClick(Event event) throws IOException {
        if (AppGlobalProperties.isLicenseKeyEntered()) {
            LIC_INFO.showAndWait();
        } else {
            licenseClick();
        }
    }

    public Stage licenseClick() throws IOException {
        if (licenseDialog != null) {
            licenseDialog.show();
            return licenseDialog;
        }
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/srs/views/licensekeyform.fxml"));
        Parent root = loader.load();
        dialog.setResizable(false);
        dialog.setTitle("Enter License Key.");
        dialog.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(root);
        dialog.setScene(scene);

        LicenseController controller = loader.getController();
        controller.setLicenseFormStage(dialog);

        dialog.show();

        licenseDialog = dialog;
        return licenseDialog;
    }

    @FXML
    public void checkSRSClick(Event event) throws IOException {
        if (checkSRSDialog != null) {
            checkSRSController.check();
            checkSRSDialog.show();
            return;
        }
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/srs/views/checksrsform.fxml"));
        Parent root = loader.load();
        dialog.setResizable(false);
        dialog.setTitle("Check SRS.");
        dialog.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(root);
        dialog.setScene(scene);

        GridPane pane = (GridPane) scene.lookup("#checkSRSForm");
        pane.getChildren().removeIf(each -> each != null);
        pane.add(new Label("Checking. Please wait..."), 0, 0, 2, 2);

        dialog.show();


        checkSRSController = loader.getController();
        checkSRSController.init(dialog, true);
        checkSRSDialog = dialog;
    }

    private SaveSRSController saveSRSController;

    private static final Alert CHECH_SRS_WARNING = new Alert(Alert.AlertType.WARNING,
            "There are problem words in responses. See Check SRS report to view details.", ButtonType.OK);
    static  {
        CHECH_SRS_WARNING.setTitle("Warning");
        CHECH_SRS_WARNING.setHeaderText("");
    }

    @FXML
    public void buildSRSClick(Event event) throws IOException {
        if (checkSRSDialog != null) {
            checkSRSDialog.show();
            Boolean reset = checkSRSController.check(true);
            if (reset == null) {
                checkSRSDialog.close();
                CHECH_SRS_WARNING.showAndWait();
                buildSRSDialog();
            } else if (reset) {
                checkSRSDialog.close();
                buildSRSDialog();
            }
            return;
        }

        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/srs/views/checksrsform.fxml"));
        Parent root = loader.load();
        dialog.setResizable(false);
        dialog.setTitle("Check SRS.");
        dialog.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(root);
        dialog.setScene(scene);

        GridPane pane = (GridPane) scene.lookup("#checkSRSForm");
        pane.getChildren().removeIf(each -> each != null);
        pane.add(new Label("Checking. Please wait..."), 0, 0, 2, 2);

        dialog.show();


        checkSRSController = loader.getController();
        checkSRSController.init(dialog, false);
        checkSRSDialog = dialog;
        if (checkSRSController.check()) {
            checkSRSDialog.close();
            buildSRSDialog();
        }
    }

    private boolean buildSRSDialog() throws IOException {
        if (buildSRSDialog != null) {
            saveSRSController.reset();
            buildSRSDialog.show();
            return true;
        }

        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/srs/views/savesrs.fxml"));
        Parent root = loader.load();
        dialog.setResizable(false);
        dialog.setTitle("Build SRS.");
        dialog.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(root);
        dialog.setScene(scene);
        dialog.show();

        saveSRSController = loader.getController();
        saveSRSController.setStage(dialog);
        buildSRSDialog = dialog;
        return false;
    }

    @FXML
    public void preferencesClick(Event event) throws IOException {
        if (preferencesDialog != null) {
            // TODO: refresh form
            preferencesController.refreshForm();
            preferencesDialog.show();
            return;
        }
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/srs/views/preferenceform.fxml"));
        Parent root = loader.load();
        dialog.setTitle("Build SRS Preferences/Admin Tools.");
        dialog.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(root);
        dialog.setScene(scene);
        dialog.show();

        preferencesController = loader.getController();
        preferencesController.setPreferencesStage(dialog);
        dialog.addEventHandler(PreferenceUpdateEvent.PREFERENCES_UPDATED, evt -> {
            mainWindow.fireEvent(evt);
        });
        preferencesController.refreshForm();
        preferencesDialog = dialog;
        preferencesDialog.setMaximized(true);
    }

    private static final Alert SAVE_SRS_ALERT = new Alert(Alert.AlertType.INFORMATION,
            "SRS has been saved.", ButtonType.OK);
    static {
        SAVE_SRS_ALERT.setHeaderText("");
        SAVE_SRS_ALERT.setTitle("");
    }
    @FXML
    public void saveSRS(Event event) throws IOException {
        AppContext context = AppContext.getInstance();
        productService.saveSRSResponses(context.getResponseList().values(), context.getProductName());
        productService.saveSRSGlossary(context.getGlossaryMap().values(), context.getProductName());
        productService.saveSRSFeatures(context.getFeatures(), context.getProductName());
        context.setEverythingStored(true);
        SAVE_SRS_ALERT.showAndWait();
    }

    @FXML
    public void assembleGlossary(Event event) {
        AppContext context = AppContext.getInstance();
        SRSConfiguration configuration = SRSConfiguration.getInstance();
        glossaryService.buildGlossary(context.getGlossaryMap(),
                context.getResponseList().values(),
                context.getFeatures(),
                configuration.getIgnoreWords(),
                configuration.getProblemWords());
        initGlossary(context.getGlossaryMap());
        context.setEverythingStored(false);
    }

    public void initGlossary(Map<String, SRSGlossary> glossaryMap) {
        fxGlossaryList.getRoot().getChildren().removeIf(o -> o != null);

        glossaryMap.values().stream().forEachOrdered(each -> {
            fxGlossaryList.getRoot().getChildren().add(new TreeItem<>(each.getTerm()));
        });
        glossaryMakeChoiceBox.setVisible(true);
    }

    public void initFeatures(List<SRSFeature> featureMap) {
        fxFeatureTreeView.getRoot().getChildren().removeIf(o -> o != null);

        featureMap.stream().forEachOrdered(each -> {
            fxFeatureTreeView.getRoot().getChildren().add(new TreeItem<>(each.getName()));
        });
    }

    @FXML
    private TextField fxFeatureNewName;
    @FXML
    private TextField fxFeatureName;
    @FXML
    private TextField fxFeaturePriority;
    @FXML
    private TextArea fxFeatureDescription;
    @FXML
    private TextArea fxFeatureResponse;
    @FXML
    private TreeView<String> fxFeatureTreeView;

    public void addNewFeature(Event event) {
        String value = fxFeatureNewName.getText();
        if (value != null && value.length() > 0) {

            SRSFeature srsFeature = new SRSFeature();
            srsFeature.setName(value);
            AppContext.getInstance().getFeatures().add(srsFeature);
            fxFeatureTreeView.getRoot().getChildren().add(new TreeItem<>(value));

            fxFeatureNewName.clear();
            AppContext.getInstance().setEverythingStored(false);
        }
    }

    private Alert EXIT_ALERT = new Alert(Alert.AlertType.WARNING,
            "Are you sure you want exit? All unsaved changes will be lost.",
            ButtonType.OK, ButtonType.CANCEL);

    @FXML
    public void close() {
        exit();
    }

    public boolean exit() {
        if (AppContext.getInstance().isEverythingStored()) {
            mainWindow.close();
            return true;
        }
        Optional<ButtonType> selection = EXIT_ALERT.showAndWait();
        if (selection.isPresent() && selection.get() == ButtonType.OK) {
            mainWindow.close();
        }
        return false;
    }

    @FXML
    public void checkSRS() {

        /*BuildSRS displays a list of SRS names from the default directory
User picks one
BuildSRS displays error message if SRS is missing or can’t be opened/accessed.
BuildSRS loads SRS from disk.
BuildSRS:
Checks Responses
Indicate if table is empty
Check that response Name is valid in all rows
Check that response is valid value in all rows
Checks Glossary
Indicate if table is empty
Check that there are no duplicate entries
Checks Features
Indicate if table is empty
Check that there are no duplicate entries
Check all text against Problem Words and report
At end;
BuildSRS shall display statistics:
Number of Glossary entries
Number of Feature entries
Number of Response entries
Number of TBD
BuildSRS does NOT save SRS to disk.*/
    }

    @FXML
    public void license() {

    }

    private List<SRSFeature> featuresMap;

    @FXML
    public void moveUp() {
        TreeItem<String> selectedItem = fxFeatureTreeView.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }
        String value = selectedItem.getValue();
        int currentIndex = fxFeatureTreeView.getSelectionModel().getSelectedIndex();
        int prevIndex = currentIndex - 1;
        if (value != null && value.length() > 0 && currentIndex > 0 && prevIndex >= 0) {
            changeFeatureSelection(currentIndex, prevIndex);
        }
    }

    @FXML
    public void moveDown() {
        TreeItem<String> selectedItem = fxFeatureTreeView.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }
        String value = selectedItem.getValue();
        int currentIndex = fxFeatureTreeView.getSelectionModel().getSelectedIndex();
        int nextIndex = currentIndex + 1;
        if (value != null && value.length() > 0 && currentIndex >= 0 && featuresMap.size() > nextIndex) {
            changeFeatureSelection(currentIndex, nextIndex);
        }
    }

    private void changeFeatureSelection(int currentIndex, int prevIndex) {
        SRSFeature selected = featuresMap.get(currentIndex);
        SRSFeature prev = featuresMap.get(prevIndex);
        featuresMap.set(prevIndex, selected);
        featuresMap.set(currentIndex, prev);

        fxFeatureTreeView.getRoot().getChildren().set(prevIndex, new TreeItem<>(selected.getName()));
        fxFeatureTreeView.getRoot().getChildren().set(currentIndex, new TreeItem<>(prev.getName()));
        fxFeatureTreeView.getSelectionModel().select(prevIndex);
    }

    @FXML
    public void open() {
        productsWindow.open();
    }
}
