package com.srs.controller;

import com.srs.app.license.LicenseService;
import com.srs.app.properties.AppGlobalProperties;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class LicenseController {
    private Stage licenseFormStage;
    @FXML
    private TextField licenseInput;

    private Alert INFORMATION = new Alert(Alert.AlertType.INFORMATION, "Your License has been accepted.", ButtonType.OK);
    private Alert ERROR = new Alert(Alert.AlertType.WARNING, "Invalid License key.", ButtonType.OK);

    @FXML
    public void okClicked(Event event) {
        String value = licenseInput.getText();
        if (value == null || value.length() == 0) {
            return;
        }
        if (LicenseService.checkLicenseKye(value)) {
            AppGlobalProperties.putLicenseKey(value);
            INFORMATION.showAndWait();
        } else {
            ERROR.showAndWait();
            return;
        }

        if (licenseFormStage != null) {
            licenseFormStage.getOnCloseRequest()
                    .handle(
                            new WindowEvent(
                                    licenseFormStage,
                                    WindowEvent.WINDOW_CLOSE_REQUEST
                            )
                    );
            licenseFormStage.close();
        }
    }

    public Stage getLicenseFormStage() {
        return licenseFormStage;
    }

    public LicenseController setLicenseFormStage(Stage licenseFormStage) {
        this.licenseFormStage = licenseFormStage;
        return this;
    }
}
