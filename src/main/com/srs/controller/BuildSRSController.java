package com.srs.controller;

import com.srs.model.ContainsSRSId;
import com.srs.model.SRSFeature;
import com.srs.model.SRSGlossary;
import com.srs.model.SRSMeta;
import com.srs.model.SRSQuestion;
import com.srs.model.SRSResponse;
import com.srs.service.SRSBuildService;
import com.srs.service.SRSBuildServiceHTMLImpl;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class BuildSRSController {
    private static final SimpleDateFormat SDF = new SimpleDateFormat("MMMM dd HH:mm:ss");
    private Stage buildSRSStage;

    @FXML
    private ListView<String> fxListView;

    private SRSBuildService buildService = SRSBuildServiceHTMLImpl.getInstance();


    public Stage getBuildSRSStage() {
        return buildSRSStage;
    }

    public void addLogMessage(String message) {
        fxListView.getItems().add(SDF.format(new Date()) + ": " + message);
    }

    public void startBuilding(SRSMeta info, List<SRSQuestion> questions,
                              Map<ContainsSRSId, SRSResponse> responses, Map<String, SRSGlossary> glossary,
                              Collection<SRSFeature> features) {
        buildService.build(info,
                questions,
                responses,
                glossary,
                features,
                logMsg -> addLogMessage(logMsg));
    }

    public void reset() {
        fxListView.getItems().removeIf(o -> o != null);
    }

    public BuildSRSController setBuildSRSStage(Stage buildSRSStage) {
        this.buildSRSStage = buildSRSStage;

        return this;
    }

    @FXML
    public void close() {
        buildSRSStage.close();
    }
}
