package com.srs.controller;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.stage.Stage;

public class AboutController {
    private Stage aboutFormStage;

    @FXML
    public void okClicked(Event event) {
        if (aboutFormStage != null) {
            aboutFormStage.close();
        }
    }

    public void setStage(Stage aboutFormStage) {
        this.aboutFormStage = aboutFormStage;
    }
}
