package com.srs.controller;

import com.srs.app.AppContext;
import com.srs.config.SRSConfiguration;
import com.srs.views.SelectProductForm;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.input.InputEvent;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class SelectProductController {
    private Stage selectProductStage;
    @FXML
    private Button selectProductOK;
    @FXML
    private Button selectProductCancel;
    @FXML
    private ComboBox<String> selectProductBox;
    private static final Alert SELECT_PRODUCT_ALERT = new Alert(Alert.AlertType.WARNING,
            "Select Existing Product or enter new one.", ButtonType.OK);
    private static final Alert PRODUCT_ALREADY_EXIST_ALERT = new Alert(Alert.AlertType.WARNING,
            "Product you are trying to create already exist. Choose different name or select product from drop down list", ButtonType.OK);

    @FXML
    public void productSelected(ActionEvent event) throws IOException {
        SingleSelectionModel<String> selectionModel = selectProductBox.getSelectionModel();
        String selectedItem;
        if (selectProductBox.getValue() != null && selectProductBox.getValue().trim().length() > 0) {
            selectedItem =  selectProductBox.getValue().trim();
        } else {
            SELECT_PRODUCT_ALERT.showAndWait();
            return;
        }

        File productDir = Paths.get(SRSConfiguration.getInstance().getDefaultDirectory(), selectedItem).toFile();
        if (!productDir.exists()) {
            // means new product. need to create new folder for product
            // and product descriptor as well
            File productDescriptor = Paths.get(SRSConfiguration.getInstance().getDefaultDirectory(),
                    selectedItem, "product.properties").toFile();

            if (!productDir.exists()) {
                productDir.mkdir();
            }
            if (productDescriptor.exists()) {
                PRODUCT_ALREADY_EXIST_ALERT.showAndWait();
                return;
            }
            productDescriptor.createNewFile();
        }

        selectProductBox.fireEvent(new InputEvent(selectProductBox, selectProductStage, SelectProductForm.PRODUCT_SELECTED));
        selectProductStage.close();
    }

    public void setFocus() {
        selectProductCancel.requestFocus();
    }

    @FXML
    public void cancel(ActionEvent event) {
        try {
            AppContext.getInstance();
            if (selectProductStage != null) selectProductStage.close();
        } catch (Exception ex) {
            System.exit(0);
        }
    }

    public Stage getSelectProductStage() {
        return selectProductStage;
    }

    public SelectProductController setSelectProductStage(Stage selectProductStage) {
        this.selectProductStage = selectProductStage;
        return this;
    }
}
