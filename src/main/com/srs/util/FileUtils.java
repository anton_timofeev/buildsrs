package com.srs.util;

import com.srs.Constants;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public abstract class FileUtils {
    private FileUtils() {

    }

    public static <T> List<T> loadCSV(Path filePath, String splitValue, Function<String[], T> rowMapper, int columns) {
        List<T> resultList = new ArrayList<>();

        String line;
        String cvsSplitBy = splitValue;

        File file = getFile(filePath);
        if (file == null) {
            return Collections.EMPTY_LIST;
        }

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            boolean isHeader = true;
            while ((line = br.readLine()) != null) {
                if (isHeader) {
                    isHeader = false;
                    continue;
                }
                String[] row = line.split(Constants.CSV_DELIMETER, columns);

                resultList.add(rowMapper.apply(row));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resultList;
    }

    public static List<String> loadSingleColumnTable(Path filePath) {
        List<String> resultList = new ArrayList<>();

        String line;

        File file = getFile(filePath);
        if (file == null) {
            return Collections.EMPTY_LIST;
        }

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            while ((line = br.readLine()) != null) {
                resultList.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Collections.sort(resultList, Comparator.naturalOrder());
        return resultList;
    }

    private static File getFile(Path filePath) {
        File file = filePath.toFile();
        if (!file.exists()) {
            return null;
        }
        return file;
    }
}
