package com.srs.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DataUtils {
    public static List<String> splitValue(String value, String splitBy) {
        return value == null || value.length() == 0 ? Collections.EMPTY_LIST : Arrays.asList(value.split(splitBy));
    }

    public static boolean convertLiteral(String value) {
        return !"no".equalsIgnoreCase(value);
    }
}
