package com.srs.views;

import com.srs.model.SRSQuestion;
import javafx.collections.ObservableList;
import javafx.event.EventType;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputEvent;
import javafx.scene.layout.HBox;

public class DeletableCustomTableCell extends TreeTableCell<SRSQuestion, Button> {

    private Runnable deleteHandler;

    public void addDeleteButtonActionHandler(Runnable r) {
        deleteHandler = r;
    }

    @Override
    protected void updateItem(Button item, boolean empty) {
        super.updateItem(item, empty);

        // If the cell is empty we don't show anything.
        if (isEmpty()) {
            setGraphic(null);
            setText(null);
        } else {
            // We only show the custom cell if it is a leaf, meaning it has
            // no children.

            // A custom HBox that will contain your check box, label and
            // button.
            HBox cellBox = new HBox(10);

            Button button = new Button();
            button.setTooltip(new Tooltip("Remove record."));

            ImageView imageView = new ImageView(DeletableCustomCell.IMAGE);
            imageView.setFitWidth(20);
            imageView.setFitHeight(20);
            button.setPrefWidth(20);
            button.setPrefHeight(20);
            button.setGraphic(imageView);
            button.setMaxWidth(20);
            button.setMaxHeight(20);
            // button.setOnAction(event -> deleteHandler.run());
            // Here we bind the pref height of the label to the height of the checkbox. This way the label and the checkbox will have the same size.

            button.setId("delete");
            cellBox.getChildren().addAll(button/*, label*/);
            TreeTableCell thisItem = this;
            button.setOnAction(event -> {
                TreeItem treeItem = thisItem.getTreeTableRow().getTreeItem();
                ObservableList children = thisItem.getTreeTableRow().getTreeTableView().getRoot().getChildren();
                int index = children.indexOf(treeItem);
                children.remove(treeItem);
                this.fireEvent(new CustomEvent(index));
            });

            // We set the cellBox as the graphic of the cell.
            setGraphic(cellBox);
        }
    }
}
