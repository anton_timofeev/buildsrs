package com.srs.views;

import com.srs.app.AppContext;
import com.srs.config.SRSConfiguration;
import com.srs.controller.SelectProductController;
import com.sun.javafx.collections.ObservableListWrapper;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.input.InputEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SelectProductForm {
    public static final EventType<InputEvent> PRODUCT_SELECTED = new EventType<>("productSelected");
    public static final EventType<InputEvent> OPEN_OTHER_PRODUCT = new EventType<>("openOtherProduct");
    private Stage stage;

    private SelectProductController selectProductController;

    public SelectProductForm(Stage parent) {

        List<String> products = getProducts(SRSConfiguration.getInstance().getDefaultDirectory());

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/srs/views/selectproductform.fxml"));
        Parent dialogRoot = null;
        try {
            dialogRoot = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage dialog = new Stage();
        this.stage = dialog;
        selectProductController = loader.getController();
        selectProductController.setSelectProductStage(stage);

        dialog.initOwner(parent);
        dialog.setResizable(false);
        dialog.setTitle("Select Product.");
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setScene(new Scene(dialogRoot, 400, 158));
        dialog.setOnShowing(event -> {
            selectProductController.setFocus();
        });
        dialog.setOnCloseRequest(event -> {
            event.consume();
            selectProductController.cancel(null);
        });
        ComboBox comboBox = (ComboBox) dialogRoot.lookup("#selectProductBox");
        comboBox.setItems(new ObservableListWrapper(products));
        dialog.show();
        // dialog.setScene(new Scene(dialogRoot, 400, 158));
/*
        dialog.setOnHiding(event -> {
            AppContext context = null;

            try {
                context = AppContext.getInstance();
            } catch (Exception ex) {

            }
            if (context != null && context.getProductName().equals(comboBox.getValue())) {

            } else if (comboBox.getSelectionModel() != null
                    && comboBox.getSelectionModel().getSelectedIndex() >= 0
                    || comboBox.getValue() != null
                    && ((String) comboBox.getValue()).trim().length() > 0) {
                comboBox.fireEvent(new InputEvent(comboBox, parent, PRODUCT_SELECTED));
            } else {

            }
        });*/
    }

    public void open() {
        List<String> products = getProducts(SRSConfiguration.getInstance().getDefaultDirectory());
        ComboBox comboBox = (ComboBox) stage.getScene().lookup("#selectProductBox");
        comboBox.setItems(new ObservableListWrapper(products));

        stage.show();
    }

    private List<String> getProducts(String folder) {
        File defaultDir = new File(folder);
        if (!defaultDir.exists()) {
            throw new IllegalStateException("Default Directory doesn't exist");
        } else if (!defaultDir.isDirectory()) {
            throw new IllegalStateException("Default Directory is file. Should be a folder.");
        }

        return Arrays.stream(defaultDir.listFiles())
                .filter(file -> file.isDirectory())
                .map(file -> {
                    File[] productDescriptors = file.listFiles(f -> f.getName().equalsIgnoreCase("product.properties"));
                    if (productDescriptors.length == 0) {
                        return null;
                    }
                    return file.getName();
                })
                .filter(productName -> productName != null)
                .collect(Collectors.toList());
    }

    public void setEventListener(EventType eType, EventHandler eHandler) {
        stage.addEventHandler(eType, eHandler);
    }
}
