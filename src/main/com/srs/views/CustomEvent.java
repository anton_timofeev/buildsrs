package com.srs.views;

import javafx.event.Event;
import javafx.event.EventType;

public class CustomEvent extends Event {

    private int parameter;

    public static final EventType<CustomEvent> TABLE_ROW_DELETED = new EventType<>("tableRowDeleted");

    public CustomEvent(int index) {
        super(CustomEvent.TABLE_ROW_DELETED);
        this.parameter = index;
    }

    public int getParameter() {
        return this.parameter;
    }

}
