package com.srs.views;

import javafx.event.Event;
import javafx.event.EventType;

public class PreferenceUpdateEvent extends Event {

    public static final EventType<PreferenceUpdateEvent> PREFERENCES_UPDATED = new EventType<>("preferencesUpdated");

    public PreferenceUpdateEvent() {
        super(PreferenceUpdateEvent.PREFERENCES_UPDATED);
    }
}
