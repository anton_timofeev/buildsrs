package com.srs.views;

import com.srs.app.AppContext;
import com.srs.app.properties.AppGlobalProperties;
import com.srs.config.SRSConfiguration;
import com.srs.config.SRSProperties;
import com.srs.controller.MainController;
import com.srs.model.ContainsSRSId;
import com.srs.model.SRSQuestion;
import com.srs.model.SRSResponse;
import com.sun.javafx.collections.ObservableListWrapper;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.control.CheckComboBox;
import org.controlsfx.control.IndexedCheckModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class MainForm {

    AppContext context;
    private MainController mainController;
    private Stage stage;
    private Map<Integer, Accordion> questionPages = new TreeMap();
    private Map<SRSQuestion, TitledPane> answerForms = new LinkedHashMap<>();

    private final ButtonType enterLicense = new ButtonType("Enter license");
    private final ButtonType continueTrial = new ButtonType("Continue trial");
    private Alert LICENSE_ALERT = new Alert(Alert.AlertType.INFORMATION,
            AppGlobalProperties.getAmoutOfFreeDays() + " days of free trial left.",
            enterLicense, continueTrial);

    public MainForm(Stage stage) throws IOException {
        LICENSE_ALERT.setTitle("Trial");
        LICENSE_ALERT.setHeaderText("");
        this.stage = stage;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/srs/views/mainform.fxml"));
        Parent root = loader.load();
        mainController = loader.getController();
        mainController.setMainWindow(stage);
        stage.setTitle("Build SRS");
        stage.setMaximized(true);
        Scene mainScene = new Scene(root);
        stage.setScene(mainScene);
        String css = this.getClass().getResource("/groupbox.css").toExternalForm();
        mainScene.getStylesheets().add(css);

        // check license
        if (!AppGlobalProperties.isLicenseKeyEntered()) {
            if (AppGlobalProperties.getAmoutOfFreeDays() <= 0) {
                enterLicenseKeyThenShowStage(stage);
            } else {
                Optional<ButtonType> response = LICENSE_ALERT.showAndWait();
                if (response.isPresent()) {
                    if (response.get() == continueTrial) {
                        showMainStage(stage);
                    } else {
                        enterLicenseKeyThenShowStage(stage);
                    }
                } else {
                    showMainStage(stage);
                }
            }
        } else {
            showMainStage(stage);
        }
    }

    private void enterLicenseKeyThenShowStage(Stage stage) throws IOException {
        mainController.licenseClick().setOnCloseRequest(event -> {
            if (AppGlobalProperties.isLicenseKeyEntered()) {
                showMainStage(stage);
            } else {
                stage.close();
            }
        });
    }

    private void showMainStage(Stage stage) {
        stage.setOnCloseRequest(event -> {
            if (!mainController.exit()) {
                event.consume();
            }
        });
        this.stage.setOnShown(event -> {
            SRSConfiguration.getInstance();
            SelectProductForm dialog = promptUserSelectProduct();
            mainController.setProductsWindow(dialog);
            dialog.setEventListener(SelectProductForm.PRODUCT_SELECTED, evt -> {
                ComboBox<String> productBox = (ComboBox) evt.getTarget();
                String productValue = productBox.getSelectionModel().getSelectedItem();
                stage.setTitle("Build SRS [Product: '" + productValue + "']");
                loadProductRelatedData(productValue);
            });

            stage.addEventHandler(SelectProductForm.OPEN_OTHER_PRODUCT, evt -> dialog.open());
        });

        stage.addEventHandler(PreferenceUpdateEvent.PREFERENCES_UPDATED, evt -> {
            refreshProductData();
        });

        stage.show();
    }

    private SelectProductForm promptUserSelectProduct() {
        // mainFormCtr.getChildren().remove(loadingLabel);
        return new SelectProductForm(this.stage);
    }

    // entry point for product selection
    private void loadProductRelatedData(String product) {
        context = AppContext.init(product);
        mainController.init();
        mainController.initGlossary(context.getGlossaryMap());
        mainController.initFeatures(context.getFeatures());
        refreshProductData();
    }

    public void refreshProductData() {
        questionPages.clear();

        List<SRSQuestion> questions = SRSConfiguration.getInstance().getQuestions();
        answerForms = questions.stream()
                .collect(Collectors.toMap(quest -> quest,
                        quest -> getTitledPane(quest, context.getResponseList()),
                        (u, v) -> {throw new IllegalStateException("");},
                        LinkedHashMap::new));

        int questionsPerPage = SRSProperties.getQuestionsPerPage();
        Pagination pagination = ((Pagination) stage.getScene().lookup("#requirementsPagination"));
        int requiredPagesAmount = questions.size() / questionsPerPage;
        pagination.setPageCount(requiredPagesAmount + (questions.size() % questionsPerPage == 0 ? 0 : 1));
        pagination.setPageFactory(param -> {
            if (questionPages.containsKey(param)) {
                return questionPages.get(param);
            }
            Accordion requirementsAccordion = new Accordion();
            requirementsAccordion.prefHeightProperty().bind(stage.heightProperty());
            requirementsAccordion.prefWidthProperty().bind(stage.widthProperty());
            int fromIndex = param * questionsPerPage;
            int toIndex = fromIndex + questionsPerPage;
            List<SRSQuestion> currentSrsQuestions = questions
                    .subList(fromIndex,
                            toIndex >= questions.size() ? questions.size() : toIndex);
            for (int i = 0; i < currentSrsQuestions.size(); i++) {
                addQuestionToForm(requirementsAccordion, currentSrsQuestions.get(i), i == 0);
            }

            questionPages.put(param, requirementsAccordion);
            return requirementsAccordion;
        });
    }

    private void addQuestionToForm(Accordion requirementsAccordion, SRSQuestion each, boolean selected) {
        TitledPane pane = answerForms.get(each);
        requirementsAccordion.getPanes().add(pane);
        // pane.setId(getPaneId(each));
        if (selected) {
            requirementsAccordion.setExpandedPane(pane);
        }
    }

    private TitledPane getTitledPane(Parent control) {
        Parent parent = control.getParent();
        if (parent instanceof TitledPane) {
            return (TitledPane) parent;
        }
        return getTitledPane(parent.getParent());
    }

    private TitledPane getTitledPane(SRSQuestion each, Map<ContainsSRSId, SRSResponse> responses) {
        SRSResponse response = responses.containsKey(each) ? responses.get(each) : null;

        GridPane content = new GridPane();
        content.setHgap(10);
        content.setVgap(10);
        content.add(new Label("Response"), 1, 1);
        Control control = null;
        SRSResponse newResponse = new SRSResponse(each);
        switch (each.getResponseType()) {
            case "T":
                TextField textField = new TextField();
                setDefaultValue(each, textField, response);
                textField.textProperty().addListener((observable, oldValue, newValue) -> {
                    SRSResponse actualResponse = response != null ? response : newResponse;
                    actualResponse.setResponse(newValue);
                    actualResponse.setActualEntry(true);
                    context.setEverythingStored(false);
                });
                control = textField;
                break;
            case "1":
                List<String> values = new ArrayList<>(each.getValues());
                if (each.isTbdAllowed()) {
                    values.add("TBD");
                }
                ComboBox<String> comboBox = new ComboBox<>(new ObservableListWrapper<>(values));
                setDefaultValue(each, comboBox, response);
                comboBox.setOnAction(event -> {
                    SRSResponse actualResponse = response != null ? response : newResponse;
                    actualResponse.setResponse(comboBox.getSelectionModel().getSelectedItem());
                    actualResponse.setActualEntry(true);
                    context.setEverythingStored(false);
                });
                control = comboBox;
                break;
            case "M":
                List<String> comboCheckBoxValues = new ArrayList<>(each.getValues());
                if (each.isTbdAllowed()) {
                    comboCheckBoxValues.add("TBD");
                }
                CheckComboBox<String> checkComboBox = new CheckComboBox<>(new ObservableListWrapper<>(comboCheckBoxValues));
                setDefaultValue(each, checkComboBox, response);
                checkComboBox.getCheckModel().getCheckedItems().addListener(new ListChangeListener<String>() {
                    public void onChanged(ListChangeListener.Change<? extends String> c) {
                        SRSResponse actualResponse = response != null ? response : newResponse;
                        actualResponse.setResponse((checkComboBox.getCheckModel().getCheckedItems().stream().map(String::valueOf).collect(Collectors.joining(","))));
                        actualResponse.setActualEntry(true);
                        context.setEverythingStored(false);
                    }
                });
                control = checkComboBox;
                break;
        }
        control.setId("response");

        content.add(control, 3, 1);
        content.add(new Label("Additional text"), 1, 2);
        TextField additionally = new TextField();
        additionally.setId("additionally");
        if (response != null) {
            additionally.setText(response.getText());
        }
        content.add(additionally, 3, 2);

        GridPane infoGrid = new GridPane();
        BorderedTitledPane infoControls = new BorderedTitledPane("Requirement information", infoGrid);
        content.add(infoControls, 1, 3, 3, 2);
        Label descriptionLabel = new Label("Response Description");
        infoGrid.add(descriptionLabel, 1, 1);
        descriptionLabel.setAlignment(Pos.TOP_LEFT);
        TextArea area = new TextArea();
        area.setId("description");
        area.setEditable(false);
        area.setText(each.getDescription());
        infoGrid.add(area, 2, 1);
        if (each.getRequirementTemplate() != null && each.getRequirementTemplate().length() > 0) {
            Label templateLabel = new Label("Response Template");
            infoGrid.add(templateLabel, 1, 2);
            templateLabel.setAlignment(Pos.TOP_LEFT);
            TextArea template = new TextArea();
            template.setId("template");
            template.setEditable(false);
            template.setText(each.getRequirementTemplate());
            infoGrid.add(template, 2, 2);
        }

        TitledPane titledPane = new TitledPane(each.getSection() + ": " + each.getName() + (each.isNotApplicableAllowed() ? "" : " *"), content);
        titledPane.setUserData(response);

        additionally.textProperty().addListener((observable, oldValue, newValue) -> {
            SRSResponse actualResponse = response != null ? response : newResponse;
            actualResponse.setText(newValue);
            actualResponse.setActualEntry(true);
            context.setEverythingStored(false);
        });

        responses.put(each, response == null ? newResponse : response);
        return titledPane;
    }

    private static class BorderedTitledPane extends StackPane {
        BorderedTitledPane(String titleString, Node content) {
            Label title = new Label(" " + titleString + " ");
            title.getStyleClass().add("bordered-titled-title");
            StackPane.setAlignment(title, Pos.TOP_LEFT);

            StackPane contentPane = new StackPane();
            content.getStyleClass().add("bordered-titled-content");
            contentPane.getChildren().add(content);

            getStyleClass().add("bordered-titled-border");
            getChildren().addAll(title, contentPane);
        }
    }

    private void setDefaultValue(SRSQuestion each, TextField textField, SRSResponse response) {
        if (response != null) {
            textField.setText(response.getResponse());
        } else if (each.getDefaultValue() != null && each.getDefaultValue().length() > 0) {
            textField.setText(each.getDefaultValue());
        }
    }

    private void setDefaultValue(SRSQuestion each, ComboBox textField, SRSResponse response) {
        if (response != null) {
            textField.getSelectionModel().select(response.getResponse());
        } else if (each.getDefaultValue() != null && each.getDefaultValue().length() > 0) {
            textField.getSelectionModel().select(each.getDefaultValue());
        }
    }

    private void setDefaultValue(SRSQuestion each, CheckComboBox textField, SRSResponse response) {
        if (response != null) {
            IndexedCheckModel checkModel = textField.getCheckModel();
            if (response.getResponse() != null) {
                checkModel.checkIndices(
                        Arrays.stream(response.getResponse().split(","))
                                .mapToInt(val -> checkModel.getItemIndex(val))
                                .toArray());
            }
        } else if (each.getDefaultValue() != null && each.getDefaultValue().length() > 0) {
            Arrays.stream(each.getDefaultValue().split(",")).forEachOrdered(val -> textField.getCheckModel().check(val));
        }
    }

    private String getPaneId(SRSQuestion each) {
        return each.getSection() + "_" + each.getName();
    }

    private SRSResponse getResponse(SRSQuestion question) {
        SRSResponse response = new SRSResponse(question);

        // questionPages.get

        return response;
    }
}
