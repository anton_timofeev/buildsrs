package com.srs.views;

import com.srs.config.ViewConstants;
import javafx.event.Event;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class DeletableCustomCell extends TreeCell<String> {
    public static final EventType<InputEvent> ITEM_DELETED = new EventType<>("itemDeleted");
    public static final Image IMAGE = new Image(DeletableCustomCell.class.getClassLoader().getResourceAsStream(("img/delete.png")));

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        // If the cell is empty we don't show anything.
        if (isEmpty()) {
            setGraphic(null);
            setText(null);
        } else {
            // We only show the custom cell if it is a leaf, meaning it has
            // no children.
            if (this.getTreeItem().isLeaf()) {

                // A custom HBox that will contain your check box, label and
                // button.
                GridPane cellBox = new GridPane();
                cellBox.minWidthProperty().bind(this.getTreeView().minWidthProperty());
                cellBox.setHgap(10);
                // HBox cellBox = new HBox(10);

                Label label = new Label(item);
                label.setId("label");
                Button button = new Button();
                button.setAlignment(Pos.TOP_RIGHT);
                // button.setOnAction(event -> deleteHandler.run());
                // Here we bind the pref height of the label to the height of the checkbox. This way the label and the checkbox will have the same size.

                button.setId("delete");
                button.setTooltip(new Tooltip("Remove item."));
                cellBox.add(label, 0, 0);
                cellBox.add(button, 1, 0);
                TreeCell thisItem = this;
                button.setVisible(false);
                Image IMAGE = DeletableCustomCell.IMAGE;

                ImageView imageView = new ImageView(IMAGE);
                imageView.setFitWidth(20);
                imageView.setFitHeight(20);
                button.setGraphic(imageView);
                button.setPrefWidth(20);
                button.setPrefHeight(20);
                button.setMaxWidth(20);
                button.setMaxHeight(20);
                this.setOnMouseMoved(event -> button.setVisible(true));
                this.setOnMouseExited(event -> button.setVisible(false));
                // cellBox.setOnMouseDragEntered(event -> button.setVisible(true));
                button.setOnAction(event -> {
                    Parent parent = thisItem.getTreeView();
                    if (parent instanceof TreeView) {
                        ((TreeView) parent).getRoot().getChildren().remove(thisItem.getTreeItem());
                        this.fireEvent(new InputEvent(this, this.getTreeItem(), ITEM_DELETED));
                    }
                });

                // We set the cellBox as the graphic of the cell.
                setGraphic(cellBox);
                // setText(item);
            } else {
                // If this is the root we just display the text.
                setGraphic(null);
                setText(item);
            }
        }
    }
}
