package com.srs.model;

import java.util.Objects;

public class ContainsSRSId {
    private String section;
    private String name;

    public String getSection() {
        return section;
    }

    public ContainsSRSId setSection(String section) {
        this.section = section;
        return this;
    }

    public String getName() {
        return name;
    }

    public ContainsSRSId setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContainsSRSId)) return false;
        ContainsSRSId question = (ContainsSRSId) o;
        return Objects.equals(section, question.section) &&
                Objects.equals(name, question.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(section, name);
    }
}
