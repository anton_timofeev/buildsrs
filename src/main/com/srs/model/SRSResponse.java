package com.srs.model;

import com.srs.Constants;
import org.apache.commons.lang3.StringEscapeUtils;

public class SRSResponse extends ContainsSRSId {
    // private SRSQuestion question;

    private boolean isActualEntry;

    private String response;

    private String text;
    public SRSResponse() {

    }

    public SRSResponse(SRSQuestion question) {
        // this.question = question;
        this.setName(question.getName());
        this.setSection(question.getSection());
        if (question.getDefaultValue() != null && question.getDefaultValue().length() > 0) {
            this.response = question.getDefaultValue();
        }
    }

    public boolean isActualEntry() {
        return isActualEntry;
    }

    public SRSResponse setActualEntry(boolean actualEntry) {
        isActualEntry = actualEntry;
        return this;
    }

    public String getResponse() {
        return response;
    }

    public SRSResponse setResponse(String response) {
        this.response = response;
        return this;
    }

    public String getText() {
        return text;
    }

    public SRSResponse setText(String text) {
        this.text = text;
        return this;
    }

    private static final String HEADER = "Section" + Constants.CSV_DELIMETER +
            "Name" + Constants.CSV_DELIMETER +
            "Actual Entry" + Constants.CSV_DELIMETER +
            "Response" + Constants.CSV_DELIMETER +
            "Text" +
            Constants.NEW_LINE_DELIMETER;

    public static String getHeader() {
        return HEADER;
    }

    @Override
    public String toString() {
        return new StringBuilder(this.getSection() != null ? StringEscapeUtils.escapeJava(this.getSection()) : "").append(Constants.CSV_DELIMETER)
                .append(this.getName() != null ? StringEscapeUtils.escapeJava(this.getName()) : "").append(Constants.CSV_DELIMETER)
                .append(isActualEntry ? "Yes" : "No").append(Constants.CSV_DELIMETER)
                .append(response != null ? StringEscapeUtils.escapeJava(response) : "").append(Constants.CSV_DELIMETER)
                .append(text != null ? StringEscapeUtils.escapeJava(text) : "")
                .toString();
    }
}
