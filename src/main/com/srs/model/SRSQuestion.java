package com.srs.model;

import com.srs.Constants;
import org.apache.commons.lang3.StringEscapeUtils;

import java.util.Arrays;
import java.util.List;

public class SRSQuestion extends ContainsSRSId {
    private String responseType;
    private List<String> values;
    private String defaultValue;
    private boolean notApplicableAllowed;
    private boolean tbdAllowed;
    private String description;
    private String requirementTemplate;
    private String text;
    private String insertIntoSRSType;

    public String getInsertIntoSRSType() {
        return insertIntoSRSType;
    }

    public SRSQuestion setInsertIntoSRSType(String insertIntoSRSType) {
        this.insertIntoSRSType = insertIntoSRSType;
        return this;
    }

    public String getResponseType() {
        return responseType;
    }

    public SRSQuestion setResponseType(String responseType) {
        this.responseType = responseType;
        return this;
    }

    public List<String> getValues() {
        return values;
    }

    public SRSQuestion setValues(List<String> values) {
        this.values = values;
        return this;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public SRSQuestion setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    public boolean isNotApplicableAllowed() {
        return notApplicableAllowed;
    }

    public SRSQuestion setNotApplicableAllowed(boolean notApplicableAllowed) {
        this.notApplicableAllowed = notApplicableAllowed;
        return this;
    }

    public boolean isTbdAllowed() {
        return tbdAllowed;
    }

    public SRSQuestion setTbdAllowed(boolean tbdAllowed) {
        this.tbdAllowed = tbdAllowed;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public SRSQuestion setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getRequirementTemplate() {
        return requirementTemplate;
    }

    public SRSQuestion setRequirementTemplate(String requirementTemplate) {
        this.requirementTemplate = requirementTemplate;
        return this;
    }

    public String getText() {
        return text;
    }

    public SRSQuestion setText(String text) {
        this.text = text;
        return this;
    }

    public boolean validateState() {
        return !(getName() == null || getName().length() == 0
                || getSection() == null || getSection().length() == 0
                // || description == null || description.length() == 0
                || (Arrays.asList("P", "B").contains(insertIntoSRSType) && (requirementTemplate == null
                                    || requirementTemplate.length() == 0)));
    }

    private static final String HEADER = "Section" + Constants.CSV_DELIMETER +
            "Name" + Constants.CSV_DELIMETER +
            "Response type" + Constants.CSV_DELIMETER +
            "Values" + Constants.CSV_DELIMETER +
            "Default value" + Constants.CSV_DELIMETER +
            "Not Applicable allowed" + Constants.CSV_DELIMETER +
            "TBD allowed" + Constants.CSV_DELIMETER +
            "Description" + Constants.CSV_DELIMETER +
            "Requirement template" + Constants.CSV_DELIMETER +
            "Text" + Constants.CSV_DELIMETER +
            "Insert into SRS type" +
            Constants.NEW_LINE_DELIMETER;

    public static String getHeader() {
        return HEADER;
    }

    @Override
    public String toString() {
        return new StringBuilder(this.getSection() == null ? "" : StringEscapeUtils.escapeJava(this.getSection())).append(Constants.CSV_DELIMETER)
                .append(this.getName() == null ? "" : StringEscapeUtils.escapeJava(this.getName())).append(Constants.CSV_DELIMETER)
                .append(responseType == null ? "" : StringEscapeUtils.escapeJava(responseType)).append(Constants.CSV_DELIMETER)
                .append(values == null ? "" : StringEscapeUtils.escapeJava(String.join(",", values))).append(Constants.CSV_DELIMETER)
                .append(defaultValue == null ? "" : StringEscapeUtils.escapeJava(defaultValue)).append(Constants.CSV_DELIMETER)
                .append(notApplicableAllowed ? "Yes" : "No").append(Constants.CSV_DELIMETER)
                .append(tbdAllowed ? "Yes" : "No").append(Constants.CSV_DELIMETER)
                .append(description == null ? "" : StringEscapeUtils.escapeJava(description)).append(Constants.CSV_DELIMETER)
                .append(requirementTemplate == null ? "" : StringEscapeUtils.escapeJava(requirementTemplate)).append(Constants.CSV_DELIMETER)
                .append(text == null ? "" : StringEscapeUtils.escapeJava(text)).append(Constants.CSV_DELIMETER)
                .append(insertIntoSRSType == null ? "" : StringEscapeUtils.escapeJava(insertIntoSRSType))
                .toString();
    }
}
