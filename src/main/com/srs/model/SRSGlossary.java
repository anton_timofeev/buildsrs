package com.srs.model;

import com.srs.Constants;
import org.apache.commons.lang3.StringEscapeUtils;

public class SRSGlossary {
    private String term;

    private String definition;

    public SRSGlossary() {

    }

    public SRSGlossary(String term) {
        this.term = term;
    }

    public String getTerm() {
        return term;
    }

    public SRSGlossary setTerm(String term) {
        this.term = term;
        return this;
    }

    public String getDefinition() {
        return definition;
    }

    public SRSGlossary setDefinition(String definition) {
        this.definition = definition;
        return this;
    }

    private static final String HEADER = "Term" + Constants.CSV_DELIMETER +
            "Definition" +
            Constants.NEW_LINE_DELIMETER;

    public static String getHeader() {
        return HEADER;
    }

    @Override
    public String toString() {
        return new StringBuilder(term).append(Constants.CSV_DELIMETER)
                .append(definition != null ? StringEscapeUtils.escapeJava(definition) : "")
                .toString();
    }
}
