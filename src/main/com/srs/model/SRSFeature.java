package com.srs.model;

import com.srs.Constants;
import org.apache.commons.lang3.StringEscapeUtils;

import java.util.Objects;

public class SRSFeature {
    private String name;

    private String priority;

    private String description;

    private String stimulusResponse;

    public String getName() {
        return name;
    }

    public SRSFeature setName(String name) {
        this.name = name;
        return this;
    }

    public String getPriority() {
        return priority;
    }

    public SRSFeature setPriority(String priority) {
        this.priority = priority;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public SRSFeature setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getStimulusResponse() {
        return stimulusResponse;
    }

    public SRSFeature setStimulusResponse(String stimulusResponse) {
        this.stimulusResponse = stimulusResponse;
        return this;
    }

    private static final String HEADER = "Name" + Constants.CSV_DELIMETER +
            "Priority" + Constants.CSV_DELIMETER +
            "Description" + Constants.CSV_DELIMETER +
            "Stimulus Response" +
            Constants.NEW_LINE_DELIMETER;

    public static String getHeader() {
        return HEADER;
    }

    @Override
    public String toString() {
        return new StringBuilder(name != null ? StringEscapeUtils.escapeJava(name) : "").append(Constants.CSV_DELIMETER)
                .append(priority != null ? StringEscapeUtils.escapeJava(priority) : "").append(Constants.CSV_DELIMETER)
                .append(description != null ? StringEscapeUtils.escapeJava(description) : "").append(Constants.CSV_DELIMETER)
                .append(stimulusResponse != null ? StringEscapeUtils.escapeJava(stimulusResponse) : "")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SRSFeature)) return false;
        SRSFeature that = (SRSFeature) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
