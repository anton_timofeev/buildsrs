package com.srs.model;

import java.util.Date;

public class SRSMeta {
    private String file;

    private String productName;

    private Date dateCreated;

    private boolean includeNegative;

    public boolean isIncludeNegative() {
        return includeNegative;
    }

    public SRSMeta setIncludeNegative(boolean includeNegative) {
        this.includeNegative = includeNegative;
        return this;
    }

    public String getFile() {
        return file;
    }

    public SRSMeta setFile(String file) {
        this.file = file;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public SRSMeta setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public SRSMeta setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }
}
