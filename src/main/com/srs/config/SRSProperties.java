package com.srs.config;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public abstract class SRSProperties {

    private SRSProperties() {
    }

    private static final Properties PROPS_HOLDER = new Properties();

    static {

        try (InputStream propertiesFileIS = SRSProperties.class.getClassLoader().getResourceAsStream("app.properties")) {
            PROPS_HOLDER.load(propertiesFileIS);
        } catch (Exception e) {
            try (InputStream propertiesFileIS = new FileInputStream("./app.properties")) {
                PROPS_HOLDER.load(propertiesFileIS);
            } catch (Exception ex) {
                try (InputStream propertiesFileIS = SRSProperties.class.getResourceAsStream("classpath:app.properties")) {
                    PROPS_HOLDER.load(propertiesFileIS);
                } catch (Exception exc) {
                    try (InputStream propertiesFileIS = SRSProperties.class.getClassLoader().getResourceAsStream("/app.properties")) {
                        PROPS_HOLDER.load(propertiesFileIS);
                    } catch (Exception exce) {

                    }
                }
            }
        }
    }
    public static boolean isPropertiesLoaded() {
        return !PROPS_HOLDER.isEmpty();
    }


    public static String getKnowledgeBaseFile() {
        return String.valueOf(PROPS_HOLDER.get("knowledge.base"));
    }

    public static String getIgnoreWordsFile() {
        return String.valueOf(PROPS_HOLDER.get("ignore.words.file"));
    }

    public static String getProblemWordsFile() {
        return String.valueOf(PROPS_HOLDER.get("problem.words.file"));
    }

    public static String getDefaultDirectory() {
        return String.valueOf(PROPS_HOLDER.get("default.directory"));
    }

    public static String getCsvSplitValue() {
        return String.valueOf(PROPS_HOLDER.get("csv.split.value"));
    }

    public static String getAboutText() {
        return String.valueOf(PROPS_HOLDER.get("about.text"));
    }

    public static int getQuestionsPerPage() {
        return PROPS_HOLDER.contains("questions.per.page")
                ? Integer.valueOf(String.valueOf(PROPS_HOLDER.get("questions.per.page")))
                : 5;
    }
}
