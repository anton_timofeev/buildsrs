package com.srs.config;

import com.srs.Constants;
import com.srs.util.FileUtils;
import com.srs.model.SRSQuestion;
import com.srs.util.DataUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SRSConfiguration {
    private static SRSConfiguration instance;

    private List<SRSQuestion> questions = new ArrayList<>();

    private List<String> ignoreWords = new ArrayList<>();

    private List<String> problemWords = new ArrayList<>();

    private String defaultDirectory;

    private boolean everythingStored;

    private SRSConfiguration() {
    }

    public boolean isEverythingStored() {
        return everythingStored;
    }

    public SRSConfiguration setEverythingStored(boolean everythingStored) {
        this.everythingStored = everythingStored;
        return this;
    }

    public static synchronized SRSConfiguration getInstance() {
        if (instance == null) {
            instance = initialize();
        }
        return instance;
    }

    private static SRSConfiguration initialize() {
        SRSConfiguration configuration = new SRSConfiguration();

        String defaultDirectory = SRSProperties.getDefaultDirectory();
        initKnowledgeBase(configuration, defaultDirectory);

        configuration.setIgnoreWords(FileUtils
                .loadSingleColumnTable(Paths.get(defaultDirectory, SRSProperties.getIgnoreWordsFile())));
        configuration.setProblemWords(FileUtils
                .loadSingleColumnTable(Paths.get(defaultDirectory, SRSProperties.getProblemWordsFile())));
        configuration.setDefaultDirectory(defaultDirectory);

        return configuration;
    }

    public String getDefaultDirectory() {
        return defaultDirectory;
    }

    public SRSConfiguration setDefaultDirectory(String defaultDirectory) {
        this.defaultDirectory = defaultDirectory;
        return this;
    }

    private static void initKnowledgeBase(SRSConfiguration configuration, String defaultDirectory) {
        configuration.setQuestions(
                FileUtils.loadCSV(Paths.get(defaultDirectory,
                        SRSProperties.getKnowledgeBaseFile()),
                        SRSProperties.getCsvSplitValue(),
                        row -> {
                            if (row.length < 11) {
                                throw new IllegalStateException("KnowledgeBase file contains invalid row: " + String.valueOf(row));
                            }
                            SRSQuestion question = new SRSQuestion();

                            int i = 0;
                            question.setSection(StringEscapeUtils.unescapeJava(row[i++]))
                                    .setName(StringEscapeUtils.unescapeJava(row[i++]));
                            question.setResponseType(StringEscapeUtils.unescapeJava(row[i++]))
                                    .setValues(DataUtils.splitValue(row[i++], ","))
                                    .setDefaultValue(StringEscapeUtils.unescapeJava(row[i++]))
                                    .setNotApplicableAllowed(DataUtils.convertLiteral(row[i++]))
                                    .setTbdAllowed(DataUtils.convertLiteral(row[i++]))
                                    .setDescription(StringEscapeUtils.unescapeJava(row[i++]))
                                    .setRequirementTemplate(StringEscapeUtils.unescapeJava(row[i++]))
                                    .setText(StringEscapeUtils.unescapeJava(row[i++]))
                                    .setInsertIntoSRSType(row[i++]);

                            return question;
                        }, 12));
    }


    public List<SRSQuestion> getQuestions() {
        return questions;
    }

    private SRSConfiguration setQuestions(List<SRSQuestion> questions) {
        this.questions = questions;
        return this;
    }

    public List<String> getIgnoreWords() {
        return ignoreWords;
    }

    private SRSConfiguration setIgnoreWords(List<String> ignoreWords) {
        this.ignoreWords = ignoreWords;
        return this;
    }

    public List<String> getProblemWords() {
        return problemWords;
    }

    private SRSConfiguration setProblemWords(List<String> problemWords) {
        this.problemWords = problemWords;
        return this;
    }

    public void updateQuestions(List<SRSQuestion> questions) throws IOException {
        this.questions = questions;
        File questionsFile = Paths.get(getDefaultDirectory(), "knowledgebase.csv").toFile();
        try( FileWriter fw = new FileWriter(questionsFile, false)) {
            fw.write(SRSQuestion.getHeader());
            fw.write(String.join(Constants.NEW_LINE_DELIMETER, questions.stream().map(SRSQuestion::toString).collect(Collectors.toList())));
            fw.flush();
        }
    }

    public void updateProblemWords(List<String> problemWords) throws IOException  {
        this.problemWords = problemWords;
        File wordsFile = Paths.get(getDefaultDirectory(), "problemwords.lst").toFile();
        try( FileWriter fw = new FileWriter(wordsFile, false)) {
            fw.write(String.join(Constants.NEW_LINE_DELIMETER, problemWords));
            fw.flush();
        }
    }

    public void updateIgnoreWords(List<String> problemWords) throws IOException  {
        this.problemWords = problemWords;
        File wordsFile = Paths.get(getDefaultDirectory(), "ignorewords.lst").toFile();
        try( FileWriter fw = new FileWriter(wordsFile, false)) {
            fw.write(String.join(Constants.NEW_LINE_DELIMETER, problemWords));
            fw.flush();
        }
    }
}
