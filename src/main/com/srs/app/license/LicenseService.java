package com.srs.app.license;

import java.util.Arrays;
import java.util.List;

public class LicenseService {
    private static final List<String> LICENSE_KEYS = Arrays.asList("AUIHSD1-9JWJD101WD-1JIW9-111",
            "AUIHSD1-9JWJD101WD-1JIW9-111",
            "AUIHSD1-9JWJD101WD-8172H-KW1",
            "AUIHSD1-9JWJD101WD-1JIW9-A21",
            "AUIHSD1-9JWJD101WD-QCPCQ-LM1",
            "AUIHSD1-KLMLMASLLL-1JIW9-PIJ");

    public static boolean checkLicenseKye(String val) {
        return LICENSE_KEYS.contains(val);
    }
}
