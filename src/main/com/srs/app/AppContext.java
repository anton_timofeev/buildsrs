package com.srs.app;

import com.srs.model.ContainsSRSId;
import com.srs.model.SRSFeature;
import com.srs.model.SRSGlossary;
import com.srs.model.SRSResponse;
import com.srs.service.SRSProductService;

import java.util.List;
import java.util.Map;

public final class AppContext {
    private static AppContext CONTEXT;

    private String productName;

    private Map<ContainsSRSId, SRSResponse> responseList;

    private Map<String, SRSGlossary> glossaryMap;
    private List<SRSFeature> featuresMap;

    private boolean everythingStored = true;

    private static SRSProductService productService = SRSProductService.getInstance();

    private AppContext() {

    }

    public boolean isEverythingStored() {
        return everythingStored;
    }

    public AppContext setEverythingStored(boolean everythingStored) {
        this.everythingStored = everythingStored;
        return this;
    }

    public static AppContext getInstance() {
        if (CONTEXT == null) {
            throw new IllegalStateException("Context has not been initialized yet. Call init first.");
        }
        return CONTEXT;
    }

    public static AppContext init(String productName) {
        CONTEXT = new AppContext();
        CONTEXT.productName = productName;
        CONTEXT.responseList = productService.loadResponses(productName);
        CONTEXT.glossaryMap = productService.loadGlossary(productName);
        CONTEXT.featuresMap = productService.loadFeatures(productName);
        return CONTEXT;
    }

    public String getProductName() {
        return productName;
    }

    public Map<ContainsSRSId, SRSResponse> getResponseList() {
        return responseList;
    }

    public Map<String, SRSGlossary> getGlossaryMap() {
        return glossaryMap;
    }

    public List<SRSFeature> getFeatures() {
        return featuresMap;
    }
}
