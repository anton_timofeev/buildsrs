package com.srs.app;

import com.srs.config.SRSConfiguration;
import com.srs.config.SRSProperties;
import com.srs.model.ContainsSRSId;
import com.srs.service.SRSValidationService;
import com.srs.views.MainForm;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class Main extends Application {


    private static final Object obj = new Object();
    public Set<String> getQuestionsDuplicates() {
        Map<ContainsSRSId, Object> tempMap = new HashMap<>();
        Set<String> resultList = new LinkedHashSet<>();

        SRSConfiguration.getInstance().getQuestions().stream().forEachOrdered(each -> {
            if (tempMap.containsKey(each)) {
                resultList.add(each.getSection() + " " + each.getName());
            } else {
                tempMap.put(each, obj);
            }
        });

        return resultList;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Set<String> duplicates;
        if (!SRSProperties.isPropertiesLoaded()) {
            new Alert(Alert.AlertType.ERROR,
                    "Configuration information missing or cannot be accessed", ButtonType.OK).showAndWait();
        } else if (!Paths.get(SRSProperties.getDefaultDirectory(),
                SRSProperties.getKnowledgeBaseFile()).toFile().exists()) {
            new Alert(Alert.AlertType.ERROR,
                    "Knowledge Base missing or cannot be accessed", ButtonType.OK).showAndWait();
        } else if (!Paths.get(SRSProperties.getDefaultDirectory(),
                SRSProperties.getProblemWordsFile()).toFile().exists()) {
            new Alert(Alert.AlertType.ERROR,
                    "Problem Words missing or cannot be accessed", ButtonType.OK).showAndWait();
        } else if (!Paths.get(SRSProperties.getDefaultDirectory(),
                SRSProperties.getIgnoreWordsFile()).toFile().exists()) {
            new Alert(Alert.AlertType.ERROR,
                    "Ignore Words List missing or cannot be accessed", ButtonType.OK).showAndWait();
        } else if (!(duplicates = getQuestionsDuplicates()).isEmpty()) {
            new Alert(Alert.AlertType.ERROR,
                    "Duplicate requirements entries detected: " + String.join(", ", duplicates),
                    ButtonType.OK).showAndWait();
        } else {
            new MainForm(primaryStage);
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
