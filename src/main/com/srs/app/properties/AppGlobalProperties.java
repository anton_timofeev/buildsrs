package com.srs.app.properties;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public abstract class AppGlobalProperties {
    private static final long FREE_TRIAL_MAX_DAYS = 7;
    private static final String FIRST_STARTUP_DATE_PROP_KEY = "fisrt.startup";
    private static final String LICENSE_KEY = "license.key";
    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");
    private static final Properties properties = new Properties();
    private static final String userHome = System.getProperty("user.home");
    private static final Path applicationDir = Paths.get(userHome, ".buildsrs");
    private static final Path propertiesFilePath = applicationDir.resolve("app.properties");

    private AppGlobalProperties() {}

    static {


        try {
            Files.createDirectories(applicationDir);
            try {
                Files.createFile(propertiesFilePath);
            } catch (FileAlreadyExistsException e) {

            }
            properties.load(new InputStreamReader(new FileInputStream(propertiesFilePath.toFile())));

            if (properties.get(FIRST_STARTUP_DATE_PROP_KEY) == null) {
                properties.setProperty(FIRST_STARTUP_DATE_PROP_KEY, SDF.format(new Date()));

                try {
                    properties.store(new FileWriter(propertiesFilePath.toFile()), "");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isLicenseKeyEntered() {
        return properties.containsKey(LICENSE_KEY);
    }

    public static long getAmoutOfFreeDays() {
        try {
            return FREE_TRIAL_MAX_DAYS -
                    ((System.currentTimeMillis() - SDF.parse(properties.getProperty(FIRST_STARTUP_DATE_PROP_KEY)).getTime()) / 24 / 60/ 60 / 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void putLicenseKey(String licenseKey) {
        properties.setProperty(LICENSE_KEY, licenseKey);

        try {
            properties.store(new FileWriter(propertiesFilePath.toFile()), "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
