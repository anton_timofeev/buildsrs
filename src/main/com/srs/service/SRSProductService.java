package com.srs.service;

import com.srs.Constants;
import com.srs.config.SRSConfiguration;
import com.srs.config.SRSProperties;
import com.srs.model.ContainsSRSId;
import com.srs.model.SRSFeature;
import com.srs.model.SRSGlossary;
import com.srs.model.SRSQuestion;
import com.srs.model.SRSResponse;
import com.srs.util.DataUtils;
import com.srs.util.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SRSProductService {
    private static final SRSProductService INSTANCE = new SRSProductService();

    public static SRSProductService getInstance() {
        return INSTANCE;
    }

    public Map<ContainsSRSId, SRSResponse> loadResponses(String productName) {
        return FileUtils.loadCSV(Paths.get(SRSConfiguration.getInstance().getDefaultDirectory(),
                productName, "responses.csv"),
                SRSProperties.getCsvSplitValue(),
                row -> {
                    SRSResponse response = new SRSResponse();

                    int i = 0;
                    response.setSection(StringEscapeUtils.unescapeJava(row[i++]))
                            .setName(StringEscapeUtils.unescapeJava(row[i++]));
                    response.setActualEntry("Yes".equalsIgnoreCase(row[i++]));
                    response.setResponse(StringEscapeUtils.unescapeJava(row[i++]))
                            .setText(StringEscapeUtils.unescapeJava(row[i++]));

                    return response;
                }, 6).stream().filter(e -> SRSConfiguration.getInstance().getQuestions().contains(e))
                .collect(Collectors.toMap(
                o -> o,
                o -> o,
                (u, v) -> {
                    throw new IllegalStateException(String.format("Duplicate key %s", u));
                }, LinkedHashMap::new));
    }

    public Map<String, SRSGlossary> loadGlossary(String productName) {
        TreeMap<String, SRSGlossary> csvMap = FileUtils.loadCSV(Paths.get(SRSConfiguration.getInstance().getDefaultDirectory(),
                productName, "glossary.csv"),
                SRSProperties.getCsvSplitValue(),
                row -> {
                    SRSGlossary response = new SRSGlossary();

                    int i = 0;
                    response.setTerm(row[i++].toLowerCase())
                            .setDefinition(StringEscapeUtils.unescapeJava(row[i++]));

                    return response;
                }, 3).stream().collect(Collectors.toMap(SRSGlossary::getTerm, srsGlossary -> srsGlossary,
                (u, v) -> {
                    throw new IllegalStateException(String.format("Duplicate key %s", u));
                }, TreeMap::new));
        return csvMap;
    }

    public List<SRSFeature> loadFeatures(String productName) {
        Map<String, SRSFeature> csvMap = FileUtils.loadCSV(Paths.get(SRSConfiguration.getInstance().getDefaultDirectory(),
                productName, "features.csv"),
                SRSProperties.getCsvSplitValue(),
                row -> {
                    SRSFeature response = new SRSFeature();

                    int i = 0;
                    response.setName(row[i++].toLowerCase())
                            .setPriority(StringEscapeUtils.unescapeJava(row[i++]))
                            .setDescription(StringEscapeUtils.unescapeJava(row[i++]))
                            .setStimulusResponse(StringEscapeUtils.unescapeJava(row[i++]));

                    return response;
                }, 5).stream().collect(Collectors.toMap(SRSFeature::getName, val -> val,
                (u, v) -> u, LinkedHashMap::new));
        return new ArrayList<>(csvMap.values());
    }

    public void saveSRSResponses(Collection<SRSResponse> responses, String productName) throws IOException {
        // TODO: if file exists prompt user to select if he wants to overwrite previous version
        File questionsFile = Paths.get(SRSConfiguration.getInstance().getDefaultDirectory(), productName, "responses.csv").toFile();
        try (FileWriter fw = new FileWriter(questionsFile, false)) {
            fw.write(SRSResponse.getHeader());
            fw.write(String.join(Constants.NEW_LINE_DELIMETER, responses.stream().map(SRSResponse::toString).collect(Collectors.toList())));
            fw.flush();
        }
    }

    public void saveSRSGlossary(Collection<SRSGlossary> glossary, String productName) throws IOException {
        // TODO: if file exists prompt user to select if he wants to overwrite previous version
        File questionsFile = Paths.get(SRSConfiguration.getInstance().getDefaultDirectory(), productName, "glossary.csv").toFile();
        try (FileWriter fw = new FileWriter(questionsFile, false)) {
            fw.write(SRSGlossary.getHeader());
            fw.write(String.join(Constants.NEW_LINE_DELIMETER, glossary.stream().map(SRSGlossary::toString).collect(Collectors.toList())));
            fw.flush();
        }
    }

    public void saveSRSFeatures(Collection<SRSFeature> features, String productName) throws IOException {
        // TODO: if file exists prompt user to select if he wants to overwrite previous version
        File questionsFile = Paths.get(SRSConfiguration.getInstance().getDefaultDirectory(), productName, "features.csv").toFile();
        try (FileWriter fw = new FileWriter(questionsFile, false)) {
            fw.write(SRSFeature.getHeader());
            fw.write(String.join(Constants.NEW_LINE_DELIMETER, features.stream().map(SRSFeature::toString).collect(Collectors.toList())));
            fw.flush();
        }
    }
}
