package com.srs.service;

import com.srs.model.ContainsSRSId;
import com.srs.model.SRSFeature;
import com.srs.model.SRSGlossary;
import com.srs.model.SRSMeta;
import com.srs.model.SRSQuestion;
import com.srs.model.SRSResponse;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class SRSBuildServiceHTMLImpl implements SRSBuildService {
    public static SRSBuildServiceHTMLImpl INSTANCE = new SRSBuildServiceHTMLImpl();

    public static SRSBuildServiceHTMLImpl getInstance() {
        return INSTANCE;
    }

    public void build(SRSMeta info, List<SRSQuestion> questions,
                      Map<ContainsSRSId, SRSResponse> responses, Map<String, SRSGlossary> glossary,
                      Collection<SRSFeature> features, Consumer<String> logs) {
        FileWriter fw;
        Map<String, String> productInfo = responses
                .values()
                .stream()
                .filter(r -> "Product".equalsIgnoreCase(r.getSection()))
                .collect(Collectors.toMap(o -> o.getName(), o -> o.getResponse()));
        questions = questions
                .stream()
                .filter(e -> !"Product".equalsIgnoreCase(e.getSection()))
                .collect(Collectors.toList());

        try {
            fw = new FileWriter(info.getFile());
        } catch (IOException e) {
            // TODO: save to default directory
            throw new RuntimeException(e);
        }
        try {
            if (fw != null) {
                List<ContentItem> content = new ArrayList<>();
                StringBuilder body = new StringBuilder();

                logs.accept("Collecting SRS responses.");
                int lastItem = assembleResponses(info, body, content, questions, responses);
                logs.accept("Collecting SRS responses has been finished");
                logs.accept("Collecting SRS Feature.");
                lastItem = assembleFeatures(body, content, lastItem, features);
                logs.accept("Collecting SRS Feature has been finished.");
                logs.accept("Collecting SRS Glossary entries.");
                lastItem = assembleGlossary(body, content, lastItem, glossary);
                logs.accept("Collecting SRS Glossary entries has been finished.");

                logs.accept("Assembling SRS file.");
                appendRequiredHTMLHeader(fw, info);
                appendTitlePages(fw, info, productInfo);
                appendContentPage(fw, content);
                // appendEmptyRevisionHistory(fw);

                fw.append(body);

                appendRequiredHTMLFooter(fw);
                logs.accept(info.getFile() + " file has been saved.");
            }
        } catch (IOException e) {
            // TODO: save to default directory
            throw new RuntimeException(e);
        } finally {
            flushAndCloseQuitely(fw);
        }
    }

    private void flushAndCloseQuitely(FileWriter fw) {
        try {
            if (fw != null) {
                fw.flush();
            }
        } catch (Exception ex) {

        } finally {
            try {
                fw.close();
            } catch (Exception ex) {

            }
        }
    }

    private void appendRequiredHTMLHeader(FileWriter fos, SRSMeta info) throws IOException {
        fos.append("<html><head><title>")
                .append("Software Requirements Specification for ")
                .append(info.getProductName())
                .append("</title>")
                .append("<style>")
                .append(" html { width: 100%; } ")
                .append(" body { width: 70%; padding-right: 15%; padding-left: 15%; } ")
                .append(" h1, h2, h3, h4, h5 { font-weight: bold; } ")
                .append(" .bold { font-weight: bold; } ")
                .append(" .title { font-weight: bold; font-size: 24px; } ")
                .append(" .title.sub { font-weight: bold; font-size: 18px; } ")
                .append(" .right { text-align: right; } ")
                .append(" .center { text-align: center; } ")
                .append(" .fullwidth { width: 100%; } ")
                .append(" .halfwidth { width: 50%; } ")
                .append(" table { width: 100%; border-collapse: collapse; } ")
                .append(" table.borders2, table.borders2 tr td, table.borders2 tr { border: 2px solid black; } ")
                .append(" table.borders1, table.borders1 tr td, table.borders2 tr { border: 1px solid black; } ")
                .append("</style>")
                .append("</head>")
                .append("<body>");
    }

    private void appendRequiredHTMLFooter(FileWriter fos) throws IOException {
        fos.append("</body></html>");
    }

    private void appendTitlePages(FileWriter fos, SRSMeta info, Map<String, String> productInfo) throws IOException {
        fos.append("<br/><br/><br/>");
        fos.append("<p>")
                .append("<H1 class='right'>")
                    .append("<span class='halfwidth'>")
                        .append("Specification for ").append(info.getProductName())
                    .append("</span>")
                .append("</H1>")
            .append("</p>")
            .append("<p>")
                .append("<H3 class='right'>")
                    .append("<span class='halfwidth'>")
                        .append("Version ").append(productInfo.get("Version"))
                    .append("</span>")
                .append("</H3>")
            .append("</p>")
            .append("<p>")
                .append("<H3 class='right'>")
                    .append("<span class='halfwidth'>")
                        .append("Prepared by ").append(productInfo.get("Author(s)"))
                    .append("</span>")
                .append("</H3>")
            .append("</p>")
            .append("<p>")
                .append("<H3 class='right'>")
                    .append("<span class='halfwidth'>")
                        .append(new SimpleDateFormat("MMMMM dd, yyyy").format(info.getDateCreated()))
                    .append("</span>")
                .append("</H3>")
            .append("</p>")
            .append("<br/>")
            .append("<br/>")
            .append("<br/>")
            .append("<br/>")
            .append("<br/>")
        .flush();
        ;
        /*Software Requirements
Specification
for
BuildSRS
Version 1.0
Prepared by Alan Clifford
December 27, 2018*/
    }

    private void appendContentPage(FileWriter fos, List<ContentItem> content) throws IOException {
        fos.append("<p class='title'>")
                .append("Table of Contents");
            fos.append("</p>");
            fos.append("<br/>");
            fos.append("<br/>");

            fos.append("<table>");
        for (int i = 0; i < content.size(); i++) {
            ContentItem item = content.get(i);
            fos.append("<tr>");
                fos.append("<td>");
                    fos.append("<a ").append(!item.isMajor() ? "style='position: relative; left: 20px;'" : "").append(" href='#").append(item.htmlId).append("'>")
                            .append(item.isMajor() ? "<h3>" : "<h4>")
                                .append(item.sectionId).append(" ").append(item.sectionText)
                            .append(item.isMajor() ? "</h3>" : "</h4>")
                        .append("</a>");
                fos.append("</td>");
            fos.append("</tr>");
        }
            fos.append("</table>");
        fos.append("<br/>");
        fos.append("<br/>");
    }

    private void appendEmptyRevisionHistory(FileWriter fos) throws IOException {
        fos.append("<br/>")
            .append("<br/>")
            .append("<br/>")
            .append("<p>")
            .append("<p>")
                .append("<span class='title'>")
                    .append("Revision History")
                .append("</span>")
            .append("</p>")
            .append("<p>")
            .append("<table class='borders2'>")
                .append("<tr class='bold'>")
                    .append("<td>")
                        .append("Name")
                    .append("</td>")
                    .append("<td>")
                        .append("Date")
                    .append("</td>")
                    .append("<td>")
                        .append("Reason for changes")
                    .append("</td>")
                    .append("<td>")
                        .append("Version")
                    .append("</td>")
                .append("</tr>")
                .append("<tr>")
                    .append("<td>").append("&#9;")
                    .append("</td>")
                    .append("<td>")
                    .append("</td>")
                    .append("<td>")
                    .append("</td>")
                    .append("<td>")
                    .append("</td>")
                .append("</tr>")
                .append("<tr>")
                    .append("<td>").append("&#9;")
                    .append("</td>")
                    .append("<td>")
                    .append("</td>")
                    .append("<td>")
                    .append("</td>")
                    .append("<td>")
                    .append("</td>")
                .append("</tr>")
            .append("</table>")
            .append("</p>")
            .append("</p>");
        // Table
        /*Revision History
Name Date*/
    }

    /**
     * Should return number of last section added
     */
    private int assembleResponses(SRSMeta info, StringBuilder fos, List<ContentItem> content, List<SRSQuestion> questions,
                                  Map<ContainsSRSId, SRSResponse> responses) {
        Map<String, List<SRSQuestion>> sectionResponses = questions.stream()
                .collect(Collectors.groupingBy(SRSQuestion::getSection, Collectors.toList()));
        int i = 1;
        Iterator<String> iterator = sectionResponses.keySet().iterator();
        while(iterator.hasNext()) {
            String section = iterator.next();

            List<SRSQuestion> questionsList = sectionResponses.get(section);

            ContentItem item = getContentItem(content, String.valueOf(i), section, true);

            fos.append("<br/>")
                .append("<br/>")
                    .append("<p>")
                    .append("<span class='title' id='").append(item.htmlId).append("'>")
                            .append(item.sectionId).append(" ").append(section)
                        .append("</span>")
                    .append("</p>");
            int k = 0;
            for (int j = 1; j <= questionsList.size(); j++) {

                k++;
                SRSQuestion quest = questionsList.get(j - 1);
                SRSResponse resp = responses.get(quest);
                if (!info.isIncludeNegative() && "B".equalsIgnoreCase(quest.getInsertIntoSRSType()) && "No".equalsIgnoreCase(resp.getResponse())) {
                    continue;
                }
                ContentItem subItem = getContentItem(content, String.valueOf(i) + "." + String.valueOf(k), quest.getName(), false);

                fos
                    .append("<p>")
                        .append("<span class='title sub' id='").append(subItem.htmlId).append("'>")
                            .append(subItem.sectionId).append(" ").append(quest.getName())
                        .append("</span>")
                    .append("</p>");

                SRSResponse response = resp;
                if (response != null) {
                    fos.append("<p style='font-style: italic;'>");
                    if ("I".equalsIgnoreCase(quest.getInsertIntoSRSType())) {
                        fos.append(response.getResponse() == null ? "" : response.getResponse());
                    }else if ("P".equalsIgnoreCase(quest.getInsertIntoSRSType())) {
                        fos.append(response.getResponse() == null ? "" : quest.getRequirementTemplate().replace("?", response.getResponse()));
                    } else if ("B".equalsIgnoreCase(quest.getInsertIntoSRSType())) {
                        fos.append(quest.getRequirementTemplate() == null ? "" : quest.getRequirementTemplate());
                    }
                    fos.append("</p>");
                    fos.append("<p style='font-style: italic;'>");
                        fos.append(response.getText() == null ? "" : response.getText());
                    fos.append("</p>");
                }
            }

                fos.append("</p>");

            i++;
        }
        // Section is major list item with bigger font size
        // Name is under section list item
        // Response is the first paragraph
        // Additionally is the second paragraph
        // section index will be passed through assembling logic
        // use templates to populate them
        return i;
    }


    private ContentItem getContentItem(List<ContentItem> content, String id, String section, boolean isMajor) {
        ContentItem item = new ContentItem();
        item.major = isMajor;
        item.setSectionId(id);
        item.sectionText = section;
        content.add(item);
        return item;
    }

    private int assembleGlossary(StringBuilder fos, List<ContentItem> content, int index, Map<String, SRSGlossary> glossary) {
        // Table
        String title = "Glossary";
        ContentItem item = getContentItem(content, String.valueOf(index), title, true);

        appendTitleSection(fos, title, item);
        fos.append("<p>");
        fos.append("<table class='borders1'>");
        glossary.values().forEach(each -> {
            fos.append("<tr>")
                    .append("<td width='25%'>")
                        .append(each.getTerm() == null ? "" : each.getTerm())
                    .append("</td>")
                    .append("<td width='75%'>")
                        .append(each.getDefinition() == null ? "" : each.getDefinition())
                    .append("</td>")
                    .append("</tr>");
        });
        fos.append("</table>");
        fos.append("</p>");
        fos.append("</p>");
        fos.append("<br/>");
        fos.append("<br/>");
        fos.append("<br/>");

        return index;
    }

    private void appendTitleSection(StringBuilder fos, String title, ContentItem item) {
        fos.append("<br/>");
        fos.append("<br/>");
        fos.append("<p>")
                .append("<p>")
                .append("<span class='title' id='").append(item.htmlId).append("'>")
                .append(item.sectionId).append(" ").append(title)
                .append("</span>")
                .append("</p>");
    }

    private int assembleFeatures(StringBuilder fos, List<ContentItem> content, int index, Collection<SRSFeature> features) {
        String title = "System Features";
        ContentItem item = getContentItem(content, String.valueOf(index), title, true);
        appendTitleSection(fos, title, item);
        fos.append("<p>");

        int i = 1;
        Iterator<SRSFeature> iterator = features.iterator();
        while(iterator.hasNext()) {
            SRSFeature feat = iterator.next();
            String subSectionId = index + "." + i;

            ContentItem subItem = getContentItem(content, subSectionId, feat.getName(), false);
                fos.append("<br/>")
                    .append("<p>")
                    .append("<span class='title sub' id='").append(subItem.htmlId).append("'>")
                        .append(subItem.sectionId).append(" ").append(subItem.sectionText)
                    .append("</span>")
                    .append("</p>");
                ContentItem subSubItem = getContentItem(content, subSectionId + ".1", "Description and Priority", false);
                    fos.append("<p>")
                    .append("<span class='title sub' id='").append(subSubItem.htmlId).append("'>")
                        .append(subSubItem.sectionId).append(" ").append(subSubItem.sectionText)
                    .append("</span>")
                    .append("</p>");
                    fos.append("<p style='font-style: italic;'>")
                        .append("<pre style='font-style: italic;'>")
                        .append(feat.getDescription() == null ? "" : feat.getDescription())
                        .append("</pre>")
                        .append("</p>")
                        .append("<p>")
                        .append("<pre style='font-style: italic;'>")
                        .append(feat.getPriority() == null ? "" : feat.getPriority())
                        .append("</pre>")
                    .append("</p>");
                subSubItem = getContentItem(content, subSectionId + ".2", "Stimulus/Response Sequences", false);
            fos.append("<p>")
                    .append("<span class='title sub' id='").append(subSubItem.htmlId).append("'>")
                    .append(subSubItem.sectionId).append(" ").append(subSubItem.sectionText)
                    .append("</h3>")
                    .append("</p>");
            fos.append("<p>")
                    .append("<pre style='font-style: italic;'>")
                    .append(feat.getStimulusResponse() == null ? "" : feat.getStimulusResponse())
                    .append("</pre>")
                    .append("</p>");
            i++;
        }
        fos.append("</p>");

        index++;
        return index;
    }

    private static class ContentItem {
        private String sectionText;

        private String sectionId;

        private boolean major;

        private String htmlId;

        public boolean isMajor() {
            return major;
        }

        public ContentItem setMajor(boolean major) {
            this.major = major;
            return this;
        }

        public String getSectionText() {
            return sectionText;
        }

        public ContentItem setSectionText(String sectionText) {
            this.sectionText = sectionText;
            return this;
        }

        public String getSectionId() {
            return sectionId;
        }

        public ContentItem setSectionId(String sectionId) {
            this.sectionId = sectionId;
            htmlId = sectionId.replaceAll("\\.", "-");
            return this;
        }
    }
}
