package com.srs.service;

import com.srs.model.SRSFeature;
import com.srs.model.SRSGlossary;
import com.srs.model.SRSResponse;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class SRSGlossaryService {
    private static SRSGlossaryService INSTANCE = new SRSGlossaryService();

    public static SRSGlossaryService getInstance() {
        return INSTANCE;
    }

    public Map<String, SRSGlossary> buildGlossary(Map<String, SRSGlossary> oldGlossary,
                                                  Collection<SRSResponse> responses, Collection<SRSFeature> features,
                                                  Collection<String> ignoreWords, Collection<String> problemWords) {
        final Map<String, SRSGlossary> glossary = oldGlossary;
        responses.stream().forEach(each ->
                glossary.putAll(Arrays.stream((each.getText() == null ? "" : each.getText()).split("\\s"))
                        .map(String::toLowerCase)
                        .filter(val -> val != null
                                && val.length() > 0
                                && !ignoreWords.contains(val)
                                && !problemWords.contains(val)
                                && !glossary.containsKey(val))
                        .collect(Collectors.toMap(key -> key, SRSGlossary::new, (u, v) -> u)))

        );
        features.stream().forEach(each ->
                glossary.putAll(Arrays.stream((each.getDescription() == null ? "" : each.getDescription()).split("\\s"))
                        .map(String::toLowerCase)
                        .filter(val -> val != null
                                && val.length() > 0
                                && !ignoreWords.contains(val)
                                && !problemWords.contains(val)
                                && !glossary.containsKey(val))
                        .collect(Collectors.toMap(key -> key, SRSGlossary::new, (u, v) -> u)))

        );
        features.stream().forEach(each ->
                glossary.putAll(Arrays.stream((each.getStimulusResponse() == null ? "" : each.getStimulusResponse()).split("\\s"))
                        .map(String::toLowerCase)
                        .filter(val -> val != null
                                && val.length() > 0
                                && !ignoreWords.contains(val)
                                && !problemWords.contains(val)
                                && !glossary.containsKey(val))
                        .collect(Collectors.toMap(key -> key, SRSGlossary::new, (u, v) -> u)))

        );

        return oldGlossary;
    }
}
