package com.srs.service;

import com.srs.model.ContainsSRSId;
import com.srs.model.SRSFeature;
import com.srs.model.SRSGlossary;
import com.srs.model.SRSMeta;
import com.srs.model.SRSQuestion;
import com.srs.model.SRSResponse;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public interface SRSBuildService {
    void build(SRSMeta info, List<SRSQuestion> questions,
               Map<ContainsSRSId, SRSResponse> responses, Map<String, SRSGlossary> glossary,
               Collection<SRSFeature> features, Consumer<String> logs);
}
