package com.srs.service;

import com.srs.app.AppContext;
import com.srs.config.SRSConfiguration;
import com.srs.model.ContainsSRSId;
import com.srs.model.SRSFeature;
import com.srs.model.SRSQuestion;
import com.srs.model.SRSResponse;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class SRSValidationService {
    public static SRSValidationService INSTANCE = new SRSValidationService();

    public static SRSValidationService getInstance() {
        return INSTANCE;
    }

    private Map<ContainsSRSId, SRSQuestion> questionMap = SRSConfiguration.getInstance().getQuestions()
            .stream()
            .collect(Collectors.toMap(o -> o, a -> a, (u, v) -> u, HashMap::new));

    public List<String> validateFeatures() {
        return AppContext.getInstance().getFeatures().stream().filter(f -> f.getPriority() == null
                || f.getPriority().trim().length() == 0
                || f.getDescription() == null || f.getDescription().trim().length() == 0)
                .map(SRSFeature::getName).collect(Collectors.toList());
    }

    public List<String> getRequiredNotPopulated() {
        return questionMap.values().stream()
                .filter(each -> !each.isNotApplicableAllowed())
                .filter(each -> {
                    SRSResponse resp = AppContext.getInstance().getResponseList().get(each);
                    return resp == null || resp.getResponse() == null || resp.getResponse().length() == 0;
                })
                .map(each -> each.getSection() + " " + each.getName())
                .collect(Collectors.toList());
    }

    public List<String> getTBDNotAllowedButTBD() {
        return questionMap.values().stream()
                .filter(each -> !each.isTbdAllowed())
                .filter(each -> {
                    SRSResponse resp = AppContext.getInstance().getResponseList().get(each);
                    return resp != null && "TBD".equalsIgnoreCase(resp.getResponse());
                })
                .map(each -> each.getSection() + " " + each.getName())
                .collect(Collectors.toList());
    }

    public long getSRSResponsesAmount() {
        return AppContext.getInstance().getResponseList().values().stream().count();
    }

    public List<String> validateResponseNames() {
        return AppContext.getInstance().getResponseList().values()
                .stream()
                .filter(SRSResponse::isActualEntry)
                .filter(each -> each.getName() == null
                        || each.getName().length() == 0)
                .map(SRSResponse::getName).collect(Collectors.toList());
    }

    public List<String> validateResponses() {
        return AppContext.getInstance().getResponseList().values()
                .stream()
                .filter(SRSResponse::isActualEntry)
                .filter(each -> each.getName() == null
                        || each.getName().length() == 0)
                .map(SRSResponse::getName).collect(Collectors.toList());
    }

    public Set<String> validateFeaturesDuplications() {
        Set<String> resList = new LinkedHashSet<>();

        Map<String, Object> tempMap = new HashMap<>();
        AppContext.getInstance().getFeatures().stream().peek(each -> {
            if (tempMap.containsKey(each.getName())) {
                resList.add(each.getName());
            }
        });

        return resList;
    }

    private boolean isAnyWordFromProblemWords(SRSFeature feature) {
        return SRSConfiguration.getInstance().getProblemWords().stream()
                .filter(each -> feature.getName().contains(each)
                        || (feature.getDescription() != null && feature.getDescription().contains(each))
                        || (feature.getStimulusResponse() != null && feature.getStimulusResponse().contains(each))
                        || (feature.getPriority() != null && feature.getPriority().contains(each))).count() > 0;
    }

    private boolean isAnyWordFromProblemWords(SRSResponse response) {
        return SRSConfiguration.getInstance().getProblemWords().stream()
                .filter(each -> response.getResponse() != null && response.getResponse().contains(each)).count() > 0;
    }

    public Set<String> validateFeaturesText() {
        return AppContext.getInstance().getFeatures().stream().filter(this::isAnyWordFromProblemWords).map(each -> each.getName()).collect(Collectors.toSet());
    }

    public Set<String> validateResponseText() {
        return AppContext.getInstance().getResponseList().values()
                .stream().filter(this::isAnyWordFromProblemWords).map(each -> each.getName()).collect(Collectors.toSet());
    }
}
