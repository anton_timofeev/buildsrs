package com.srs;

public class Constants {
    public static final String NEW_LINE_DELIMETER = "\r\n";
    public static final String CSV_DELIMETER = "\t";
}
